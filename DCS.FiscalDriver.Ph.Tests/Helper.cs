﻿namespace DCS.FiscalDriver.Ph.Tests
{
	
	public class Helper
    {
        
		public static DCSPOSUtility.clsPOSXaction GetTransaction(bool exempt, bool applyDiscount)
        {

            /*
             * Little explanation of the misnomers in DCSPOSUtility.clsPOSXaction:
             * Originally, we catered only for SALES-TAX (USA) tax types. Later, we had to
             * also take care of VAT-TYPE taxing. Therefore, properties and values have now
             * non-intuitive names because they were reused, changed meaning, or both.
             * To add to the confusion, I (ML) do not really know if the Items send their 
             * values (read prices) as NET, with TAX INCLUDED or a mix-n-match.
             * Modders are apart... These don't even have tax info, so POS and any driver
             * is basically in the dark regarding modder taxing.
             * 
             * The transaction object totals hierarchy is, then, the following:
             * 
             *   CommLink sends the transaction's NetTotal as the sum of all items sold and 
             *   their quantities. So: 
             *   
             *     <xactObject>.dbXactNetTotal = [commlink->NetTotal]
             *     
             *   Then, as POS must show VAT transactions including VAT, it sets:
             *   
             *     <xactObject>.dbXactSubTotal = <xactObject>.dbXactNetTotal + [VAT-Type taxes]
             *     
             *   Now, a "pesada herencia" is introduced, but it's pretty useless:
             *   
             *     <xactObject>.dbXactTotalBeforeDiscount = <xactObject>.dbXactSubTotal
             *     
             *   Then, the discounts are finally applied and non-VAT-Type taxes taken into
             *   account, all in one fell swoop:
             *   
             *       <xactObject>.dbTotalToPay = <xactObject>.Subtotal - <xactObject>.oDiscount.Amount + [Non-VAT-Type taxes]
             *       
             * Again, please note that the info above does not cover ProductInfo data nor 
             * individual LineItem data, so I (ML) cannot tell for sure how to do the maths
             * adequately for them. I can just guess.
             */

            DCSPOSUtility.clsPOSXaction myXact = new DCSPOSUtility.clsPOSXaction();
            DCSPOSUtility.clsPOSCard myCrd1 = new DCSPOSUtility.clsPOSCard();
            DCSPOSUtility.clsPOSCard myCrd2 = new DCSPOSUtility.clsPOSCard();
            DCSPOSUtility.clsPOSLineItem mylItm = new DCSPOSUtility.clsPOSLineItem();
            DCSPOSUtility.clsPOSLineItem mylIt2 = new DCSPOSUtility.clsPOSLineItem();
            DCSPOSUtility.clsPOSDiscount myDsct = new DCSPOSUtility.clsPOSDiscount();
            DCSPOSUtility.clsPOSTax myTax = new DCSPOSUtility.clsPOSTax();
            DCSPOSUtility.clsPOSTax myXactTax = new DCSPOSUtility.clsPOSTax();
            DCSPOSUtility.collPOSTaxes myTaxes = new DCSPOSUtility.collPOSTaxes();
            DCSPOSUtility.clsPOSProductModifier myModder = new DCSPOSUtility.clsPOSProductModifier();
            DCSPOSUtility.clsPOSPaymode myPMode = new DCSPOSUtility.clsPOSPaymode();
            DCSPOSUtility.clsPOSPayment myPmnt = new DCSPOSUtility.clsPOSPayment();

            //Define some tax.
            myTax.Create(1, "VAT", 12, 0, 0, DCSPOSUtility.enuTaxType.tt_Discriminated);

            //Tax collection holding one tax
            myTaxes.Add(myTax);

            //A single LineItem
            mylItm.ID = 15;
            mylItm.Description = "LineItem #1";
            mylItm.IsNormalSale = true;
            mylItm.NonDeletable = false;
            mylItm.Quantity = 1;
            mylItm.UPrice = (decimal)112.00;

            //Another LineItem
            mylIt2.ID = 16;
            mylIt2.Description = "LineItem #2";
            mylIt2.IsNormalSale = true;
            mylIt2.NonDeletable = false;
            mylIt2.Quantity = 10;
            mylIt2.UPrice = (decimal)1.12;

            //A single Modifier
            myModder.ID = 17;
            myModder.Description = "Modder #1";
            myModder.Price = (decimal)1.12;

            //LineItem's ProductInfo, applying above tax(es)
            mylItm.ProductInfo.Create(2, "ProdInfo #1", 30, "", false, false, new DCSPOSUtility.collPOSProductAttributes(),
                new DCSPOSUtility.collPOSCounterMovements(), myTaxes, new DCSPOSUtility.collPOSProductModifiers());
            mylIt2.ProductInfo.Create(2, "ProdInfo #2", 12, "", false, false, new DCSPOSUtility.collPOSProductAttributes(),
                new DCSPOSUtility.collPOSCounterMovements(), myTaxes, new DCSPOSUtility.collPOSProductModifiers());

            //Add the modders to the LineItems
            mylItm.ProductInfo.Modifiers.Add(myModder);
            mylIt2.ProductInfo.Modifiers.Add(myModder);

            //Set some special request
            mylItm.ProductInfo.Modifiers.SpecialRequest = "(Medium Rare)";

            //A single card - The NULL card
            myCrd1.CardNumber = 0;
            myCrd1.LineItems.Add(mylItm);
            myCrd1.LineItems.Add(mylIt2);     //As soon as this item is added, the driver explodes with a duplicate key when trying to print.

            //Another single card - Some card
            myCrd2.CardNumber = 12435789;
            myCrd2.LineItems.Add(mylIt2);

            //Add the cards to the transaction
            myXact.Cards.Add(myCrd1);
            myXact.Cards.Add(myCrd2);

            //Set basic transaction financials
            myXact.ID = 1342;
            myXact.IsTaxExempt = false;

            //Calculate the transaction totals taking in consideration
            //all items of all cards along with their taxes.
            //We start from the definition that all products' and modders' 
            //prices DO INCLUDE TAX when TaxMode=2 (VAT)
            decimal totalTaxBase = 0;
            decimal totalTaxAmount = 0;
            decimal taxDecimalRate = 0;

            foreach (DCSPOSUtility.clsPOSCard cd in myXact.Cards)
            {

                foreach(DCSPOSUtility.clsPOSLineItem li in cd.LineItems)
                {

                    decimal prodPrice = li.Quantity * li.UPrice;

                    foreach (DCSPOSUtility.clsPOSProductModifier md in li.ProductInfo.Modifiers)
                    {
                        prodPrice += li.Quantity * md.Price;
                    }

                    foreach(DCSPOSUtility.clsPOSTax tx in li.ProductInfo.Taxes)
                    {

                        if (DCSPOSUtility.enuTaxType.tt_Discriminated == tx.TaxType)
                        {
                            taxDecimalRate = tx.TaxRate / 100;
                            decimal taxBase = prodPrice / (1 + taxDecimalRate);
                            decimal taxAmount = prodPrice - taxBase;
                            totalTaxBase += taxBase;
                            totalTaxAmount += taxAmount;
                        }

                    }

                }

            }

            //Add tax to transaction
            myXactTax.Create(1, "VAT", 12, totalTaxAmount, totalTaxBase, DCSPOSUtility.enuTaxType.tt_Discriminated);
            myXact.cTaxes.Add(myXactTax);

            //Add transaction financial data
            myXact.dbXactNetTotal = totalTaxBase;
            myXact.dbXactSubtotal = (exempt ? 
                                     myXact.dbXactNetTotal : 
                                     myXact.dbXactNetTotal + totalTaxAmount
                                    );
            if (applyDiscount)
            {
                myDsct.Amount = (myXact.dbXactSubtotal - (myXact.dbXactSubtotal / (decimal)1.20));   //Create a discount of 20%
                myDsct.ID = 1;
                myDsct.Name = "Discount 20%";
                myDsct.RequiresID = false;
            }
            myXact.oDiscount = myDsct;
            myXact.dbXactTotalBeforeDiscount = myXact.dbXactSubtotal;
            myXact.dbXactTotalToPay = myXact.dbXactTotalBeforeDiscount - myXact.oDiscount.Amount; //No sales-tax type taxes here to be added!

            //Create a payment for HALF the total to pay, and $1 in change
            myPMode.Description = "Cash";
            myPMode.ID = 1;
            myPMode.InputMode = DCSPOSUtility.enuInputMode.im_00_Unknown;
            myPMode.NeedsPIN = false;
            myPMode.PMType = DCSPOSUtility.enuPModeType.pmt_01_Cash;
            myPMode.XchgRatio = 1;
            myPmnt.Amount = myXact.dbXactTotalToPay / 2;
            myPmnt.Paymode = myPMode;
            myPmnt.PaymodeID = myPMode.ID;
            myPmnt.Tendered = ((int)(myXact.dbXactTotalToPay / 2)) + 1;
            myPmnt.Change = myPmnt.Tendered - myPmnt.Amount;

            //Add the payment TWICE (so the transaction is paid in full)
            myXact.Payments.Add(myPmnt);
            myXact.Payments.Add(myPmnt);

            //Finally, return the transaction
            return myXact;

        }
    }

}
