﻿namespace DCS.FiscalDriver.Ph.Tests
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnGetStatus = new System.Windows.Forms.Button();
            this.fmePhilippines = new System.Windows.Forms.GroupBox();
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.txtZRead = new System.Windows.Forms.TextBox();
            this.cmdZRead = new System.Windows.Forms.Button();
            this.cmdPrintXact = new System.Windows.Forms.Button();
            this.fmePhilippines.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Location = new System.Drawing.Point(12, 12);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 0;
            this.btnPrint.Text = "Print!";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnGetStatus
            // 
            this.btnGetStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetStatus.Location = new System.Drawing.Point(93, 12);
            this.btnGetStatus.Name = "btnGetStatus";
            this.btnGetStatus.Size = new System.Drawing.Size(75, 23);
            this.btnGetStatus.TabIndex = 1;
            this.btnGetStatus.Text = "Get Status";
            this.btnGetStatus.UseVisualStyleBackColor = true;
            this.btnGetStatus.Click += new System.EventHandler(this.btnGetStatus_Click);
            // 
            // fmePhilippines
            // 
            this.fmePhilippines.Controls.Add(this.txtInvNo);
            this.fmePhilippines.Controls.Add(this.txtZRead);
            this.fmePhilippines.Controls.Add(this.cmdZRead);
            this.fmePhilippines.Controls.Add(this.cmdPrintXact);
            this.fmePhilippines.Enabled = false;
            this.fmePhilippines.Location = new System.Drawing.Point(12, 59);
            this.fmePhilippines.Name = "fmePhilippines";
            this.fmePhilippines.Size = new System.Drawing.Size(190, 101);
            this.fmePhilippines.TabIndex = 2;
            this.fmePhilippines.TabStop = false;
            this.fmePhilippines.Text = "Fiscal Philippines";
            // 
            // txtInvNo
            // 
            this.txtInvNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInvNo.Location = new System.Drawing.Point(134, 22);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.ReadOnly = true;
            this.txtInvNo.Size = new System.Drawing.Size(50, 22);
            this.txtInvNo.TabIndex = 1;
            this.txtInvNo.Text = "-?-";
            this.txtInvNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtZRead
            // 
            this.txtZRead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtZRead.Location = new System.Drawing.Point(134, 50);
            this.txtZRead.Name = "txtZRead";
            this.txtZRead.ReadOnly = true;
            this.txtZRead.Size = new System.Drawing.Size(50, 22);
            this.txtZRead.TabIndex = 3;
            this.txtZRead.Text = "-?-";
            this.txtZRead.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmdZRead
            // 
            this.cmdZRead.Enabled = false;
            this.cmdZRead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdZRead.Location = new System.Drawing.Point(6, 48);
            this.cmdZRead.Name = "cmdZRead";
            this.cmdZRead.Size = new System.Drawing.Size(122, 23);
            this.cmdZRead.TabIndex = 2;
            this.cmdZRead.Text = "Print Z-Read";
            this.cmdZRead.UseVisualStyleBackColor = true;
            this.cmdZRead.Click += new System.EventHandler(this.cmdZRead_Click);
            // 
            // cmdPrintXact
            // 
            this.cmdPrintXact.Enabled = false;
            this.cmdPrintXact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPrintXact.Location = new System.Drawing.Point(6, 19);
            this.cmdPrintXact.Name = "cmdPrintXact";
            this.cmdPrintXact.Size = new System.Drawing.Size(122, 23);
            this.cmdPrintXact.TabIndex = 0;
            this.cmdPrintXact.Text = "Print Sample Transaction";
            this.cmdPrintXact.UseVisualStyleBackColor = true;
            this.cmdPrintXact.Click += new System.EventHandler(this.cmdPrintXact_Click);
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(214, 172);
            this.Controls.Add(this.fmePhilippines);
            this.Controls.Add(this.btnGetStatus);
            this.Controls.Add(this.btnPrint);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "TestForm";
            this.Text = "Philippines Tester";
            this.fmePhilippines.ResumeLayout(false);
            this.fmePhilippines.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnGetStatus;
        private System.Windows.Forms.GroupBox fmePhilippines;
        private System.Windows.Forms.Button cmdZRead;
        private System.Windows.Forms.Button cmdPrintXact;
        private System.Windows.Forms.TextBox txtZRead;
        private System.Windows.Forms.TextBox txtInvNo;
    }
}