﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DCS.FiscalDriver.Ph.Tests
{

    public partial class TestForm : Form
    {

        private DCS.FiscalDriver.Ph.Driver _driver = new DCS.FiscalDriver.Ph.Driver();

        /*
        public static void Main(string[] args)
        {
            //Dunno where the hell to put this!
        }
        */

        public TestForm()
        {

            InitializeComponent();

            //Initialize low level driver
            var initOk = _driver.Create(System.Environment.GetEnvironmentVariable("NW"));

            //Enable controls if OK, tell message if not.
            if (initOk)
            {
                fmePhilippines.Enabled = true;
                cmdPrintXact.Enabled = true;
                cmdZRead.Enabled = true;
            }
            else
            {
                MessageBox.Show($"Driver failed Create() call: {_driver.LastErrorString()}.",
                    "ERROR",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
        //    var labels = new Dictionary<string, string>();
        //    var textBlocks = new List<PrintManager.TextBlock>();
        //    var print = new PrintManager.Engine();

        //    labels.Add("DATE", DateTime.Now.ToString());
        //    labels.Add("ADDRESS", "Cabrera 5480");
        //    labels.Add("PROVINCE", "CABA");
        //    labels.Add("TOTAL", "10.10");

        //    var tb = new PrintManager.TextBlock("Products");
        //    tb.Values.Add(new Dictionary<string, object>
        //                {
        //                    {"PRODUCT_ID", "1"},
        //                    {"PRODUCT_NAME", "Test 1"},
        //                    {"PRICE", "2.10"},
        //                    {"EXTRA_PRODUCT", "Mayonesa"}
        //                });

        //    textBlocks.Add(tb);

        //    tb = new PrintManager.TextBlock("Products");
        //    tb.Values.Add(new Dictionary<string, object>
        //            {
        //                {"PRODUCT_ID", "2"},
        //                {"PRODUCT_NAME", "Test 2"},
        //                {"PRICE", "5.01"},
        //                {"EXTRA_PRODUCT", "NO_PRINT"}
        //            });

        //    textBlocks.Add(tb);

        //    print.Print(Environment.GetEnvironmentVariable("NW") + @"C:\Repo\fiscaldriver-filipinas\PrintTemplates\WinTemplate_Example.txt", labels, textBlocks);

        //}

        //private void btnGetStatus_Click(object sender, EventArgs e)
        //{
        //    PrintManager.Engine print = new PrintManager.Engine();
        //    MessageBox.Show(print.CheckStatus().ToString());

        //}

        //private void cmdPrintXact_Click(object sender, EventArgs e)
        //{

        //    DCSPOSUtility.clsPOSXaction testXact;
        //    Int32 invoiceNum = 0;

        //    //Next is a dirty trick to get a New VBA.Collection, which is NOT CREATABLE in .Net.
        //    //STEP 1 - Instance an object that holds a VBA.Collection inside...
        //    var dummy = new DCSPOSUtility.clsPOSXaction();
        //    //STEP 2 - ???
        //    //STEP 3 - Profit! Use the embedded VBA.Collection from the parent object.
        //    VBA.Collection myParams = dummy.Cards;

        //    if (_driver.OpenTransaction(ref myParams))
        //    {
        //        bool exmpt = bool.Parse(myParams.Item("TaxExempt").ToString());
        //        bool dsctd = (int.Parse(myParams.Item("DiscountId").ToString()) != 0);

        //        testXact = Helper.GetTransaction(exmpt, dsctd);
        //        MessageBox.Show("Transaction is " +
        //            (exmpt ? "exempt, " : "taxed, ") +
        //            (dsctd ? "discounted." : "not discounted."),
        //            "Xact Info",
        //            MessageBoxButtons.OK,
        //            MessageBoxIcon.Information);
        //    }
        //    else
        //    {
        //        MessageBox.Show($"Driver failed OpenTransaction() call: {_driver.LastErrorString()}.", "ERROR",
        //            MessageBoxButtons.OK,
        //            MessageBoxIcon.Error);
        //        return;
        //    }

        //    if (_driver.PrintTransaction(testXact, ref invoiceNum))
        //    {
        //        txtInvNo.Text = invoiceNum.ToString();
        //    }
        //    else
        //    {
        //        MessageBox.Show($"Driver failed PrintTransaction() call: {_driver.LastErrorString()}.",
        //            "ERROR",
        //            MessageBoxButtons.OK,
        //            MessageBoxIcon.Error);
        //        return;
        //    }

        //}

        //private void cmdZRead_Click(object sender, EventArgs e)
        //{

        //    int myZNumber = -1;
        //    bool zreadOK;

        //    zreadOK = _driver.PerformZClose(ref myZNumber);
        //    if (zreadOK)
        //    {
        //        txtZRead.Text = myZNumber.ToString();
        //    }
        //    else
        //    {
        //        txtZRead.Text = "-?-";
        //        MessageBox.Show($"Driver failed PerformZClose() call: {_driver.LastErrorString()}.",
        //            "ERROR",
        //            MessageBoxButtons.OK,
        //            MessageBoxIcon.Error);
        //        return;
        //    }

        }

    }

}