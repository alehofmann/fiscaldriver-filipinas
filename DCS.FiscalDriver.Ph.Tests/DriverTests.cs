﻿using NUnit.Framework;
using System.Windows.Forms;

namespace DCS.FiscalDriver.Ph.Tests
{
	[TestFixture]
	class DriverTests
	{

        private DCS.FiscalDriver.Ph.Dao.PhDao _dao;
		private Driver _driver;

        [SetUp]
		public void Setup()
		{
			_dao = new Dao.PhDao();
			_driver = new Driver();
			if (!_driver.Create(""))
			{
				Assert.Fail($"Driver create failed: {_driver.LastErrorString()}");				
			}

		}

		[Test]
		public void TestPrintZread()
		{			
			int zCloseNumber=0;
			Assert.IsTrue(_driver.PerformZClose(ref zCloseNumber));
			//Assert.That(zCloseNumber > 0);

		}

        [Test]
        public void TestPrintTransaction()
        {

            DCSPOSUtility.clsPOSXaction testXact;
            int invoiceNum = 0;

            //Next is a dirty trick to get a New VBA.Collection, which is NOT CREATABLE in .Net.
            //STEP 1 - Instance an object that holds a VBA.Collection inside...
            var dummy = new DCSPOSUtility.clsPOSXaction();
            //STEP 2 - ???
            //STEP 3 - Profit! Use the embedded VBA.Collection from the parent object.
            VBA.Collection myParams = dummy.Cards;

            if (_driver.OpenTransaction(ref myParams))
            {
                bool exmpt = bool.Parse(myParams.Item("TaxExempt").ToString());
                bool dctid = (int.Parse(myParams.Item("DiscountId").ToString()) != 0);

                testXact = Helper.GetTransaction(exmpt, dctid);
                MessageBox.Show("Transaction is " +
                    (exmpt ? "exempt, " : "taxed, ") +
                    (dctid ? "discounted." : "not discounted."),
                    "Xact Info",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show($"Driver failed OpenTransaction() call: {_driver.LastErrorString()}.", "ERROR",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            if (_driver.PrintTransaction(testXact, ref invoiceNum))
            {
                //txtInvNo.Text = invoiceNum.ToString();
                MessageBox.Show($"Printed invoice #{invoiceNum.ToString()}", 
                    "SUCCESS!", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show($"Driver failed PrintTransaction() call: {_driver.LastErrorString()}.",
                    "ERROR",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

        }

    }
}
