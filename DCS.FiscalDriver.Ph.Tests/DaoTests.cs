﻿using System;
using NUnit.Framework;
using DCS.FiscalDriver.Ph.Domain;
using System.IO;

namespace DCS.FiscalDriver.Ph.Tests
{
	[TestFixture]
	public class DaoTests
	{

        private DCS.FiscalDriver.Ph.Dao.PhDao _dao;

        [SetUp] 
		public void Setup()
		{
			_dao = new Dao.PhDao();
		}

		[Test]
		public void TestSaveInvoiceToDb()
		{

			var sequenceFile = Path.Combine("d:\\sys\\", $"dcs\\data\\INVOICE_{Environment.MachineName}.seq");
            var pmts = new System.Collections.Generic.List<Payment>();
            var pmt = new Payment();

            pmt.AmountPaid = 250;
            pmt.AmountTendered = 500;
            pmt.AuthorizationCode = "";
            pmt.CurrencyId = 1;
            pmt.CurrencyName = "Cash";
            pmt.CurrencyType = 1;
            pmt.ExchangeRatio = 1;
            pmts.Add(pmt);

            pmt = new Payment();
            pmt.AmountPaid = 750;
            pmt.AmountTendered = 1000;
            pmt.AuthorizationCode = "A1B2C3";
            pmt.CurrencyId = 2;
            pmt.CurrencyName = "VIZA";
            pmt.CurrencyType = 2;
            pmt.ExchangeRatio = 1;
            pmts.Add(pmt);
            
            var scPwdTrans = new SalesInvoice
			{
				OperationDate = DateTime.Now,
				RegistrationNumber = 33,
				InvoiceNumber = 47,
				CustomerName = "Pedro Picapiedras",
				CustomerAddress = "Sagardúa 4040, 2do patio preguntar por Tito",
				SalesTotal = 1000,
				SalesDiscount = 200,				
				CashierTransactionId = 999,
				InvoiceId = "111",
				HypotheticVatAmount = decimal.Parse("107.14"),
				InvoiceType = SalesInvoice.InvoiceTypesEnum.Pwd,
                Payments = pmts
			};

			_dao.SaveInvoice(scPwdTrans);
            	
		}

		[Test]
		public void TestGetZRead()
		{
			var zRead = _dao.PerformZRead();
		}

	}
}
