﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Drawing;
using System.Drawing.Printing;
using System.Printing;

namespace PrintManager
{
    public class Engine
    {
        private log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private PrintDocument printDoc;


        private Font _linefont;
        private bool _lineFontDefault;
        private int _lineFontSize;
        private bool _lineFontBold;
        private bool _lineFontItalic;
        private bool _lineFontUnderline;
        private Brush _lineFontColor;
        private Brush _lineFontBackgrondColor;
        private FontFamily _lineFontFamily;
        private FontStyle _lineFontStyle;
        private StringFormat _linestringFormat;
        private Rectangle _rect;
        private int _linePosX;
        private int _linePosY;
        private int _lineWitdh;
        private bool _lineFontBarCode39;
        private string _template;

        private string _path;
        private Dictionary<string, string> _labels;
        private TextBlock _textBloqs = new TextBlock();

        public bool PrintResult { get; set; }
        public string PrintErrorMsg { get; set; }

        public 
 }

    }
}
