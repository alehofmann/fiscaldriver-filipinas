﻿namespace DCS.FiscalDriver.Ph
{
    partial class RequestFiscalDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdOK = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.fmeCustomerType = new System.Windows.Forms.GroupBox();
            this.optCustOrg = new System.Windows.Forms.RadioButton();
            this.optCustSenior = new System.Windows.Forms.RadioButton();
            this.optCustDisabled = new System.Windows.Forms.RadioButton();
            this.optCustNormal = new System.Windows.Forms.RadioButton();
            this.fmeCustomerData = new System.Windows.Forms.GroupBox();
            this.txtBenefID = new System.Windows.Forms.TextBox();
            this.txtCustID = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblBenefID = new System.Windows.Forms.Label();
            this.lblCustID = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.fmeCustomerType.SuspendLayout();
            this.fmeCustomerData.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdOK
            // 
            this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdOK.Location = new System.Drawing.Point(327, 150);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(92, 23);
            this.cmdOK.TabIndex = 2;
            this.cmdOK.Text = "Aceptar";
            this.cmdOK.UseVisualStyleBackColor = true;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(447, 150);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(92, 23);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "Cancelar";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // fmeCustomerType
            // 
            this.fmeCustomerType.Controls.Add(this.optCustOrg);
            this.fmeCustomerType.Controls.Add(this.optCustSenior);
            this.fmeCustomerType.Controls.Add(this.optCustDisabled);
            this.fmeCustomerType.Controls.Add(this.optCustNormal);
            this.fmeCustomerType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fmeCustomerType.Location = new System.Drawing.Point(12, 12);
            this.fmeCustomerType.Name = "fmeCustomerType";
            this.fmeCustomerType.Size = new System.Drawing.Size(116, 132);
            this.fmeCustomerType.TabIndex = 0;
            this.fmeCustomerType.TabStop = false;
            this.fmeCustomerType.Text = "Tipo De Cliente";
            // 
            // optCustOrg
            // 
            this.optCustOrg.AutoSize = true;
            this.optCustOrg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.optCustOrg.Location = new System.Drawing.Point(19, 100);
            this.optCustOrg.Name = "optCustOrg";
            this.optCustOrg.Size = new System.Drawing.Size(49, 17);
            this.optCustOrg.TabIndex = 3;
            this.optCustOrg.Text = "Organizaciones";
            this.optCustOrg.UseVisualStyleBackColor = true;
            this.optCustOrg.CheckedChanged += new System.EventHandler(this.optCustOrg_CheckedChanged);
            // 
            // optCustSenior
            // 
            this.optCustSenior.AutoSize = true;
            this.optCustSenior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.optCustSenior.Location = new System.Drawing.Point(19, 51);
            this.optCustSenior.Name = "optCustSenior";
            this.optCustSenior.Size = new System.Drawing.Size(37, 17);
            this.optCustSenior.TabIndex = 1;
            this.optCustSenior.Text = "SC";
            this.optCustSenior.UseVisualStyleBackColor = true;
            this.optCustSenior.CheckedChanged += new System.EventHandler(this.optCustSenior_CheckedChanged);
            // 
            // optCustDisabled
            // 
            this.optCustDisabled.AutoSize = true;
            this.optCustDisabled.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.optCustDisabled.Location = new System.Drawing.Point(19, 76);
            this.optCustDisabled.Name = "optCustDisabled";
            this.optCustDisabled.Size = new System.Drawing.Size(49, 17);
            this.optCustDisabled.TabIndex = 2;
            this.optCustDisabled.Text = "PWD";
            this.optCustDisabled.UseVisualStyleBackColor = true;
            this.optCustDisabled.CheckedChanged += new System.EventHandler(this.optCustDisabled_CheckedChanged);
            // 
            // optCustNormal
            // 
            this.optCustNormal.AutoSize = true;
            this.optCustNormal.Checked = true;
            this.optCustNormal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.optCustNormal.Location = new System.Drawing.Point(19, 26);
            this.optCustNormal.Name = "optCustNormal";
            this.optCustNormal.Size = new System.Drawing.Size(61, 17);
            this.optCustNormal.TabIndex = 0;
            this.optCustNormal.TabStop = true;
            this.optCustNormal.Text = "Normal";
            this.optCustNormal.UseVisualStyleBackColor = true;
            this.optCustNormal.CheckedChanged += new System.EventHandler(this.optCustNormal_CheckedChanged);
            // 
            // fmeCustomerData
            // 
            this.fmeCustomerData.Controls.Add(this.txtBenefID);
            this.fmeCustomerData.Controls.Add(this.txtCustID);
            this.fmeCustomerData.Controls.Add(this.txtAddress);
            this.fmeCustomerData.Controls.Add(this.txtName);
            this.fmeCustomerData.Controls.Add(this.lblBenefID);
            this.fmeCustomerData.Controls.Add(this.lblCustID);
            this.fmeCustomerData.Controls.Add(this.lblAddress);
            this.fmeCustomerData.Controls.Add(this.lblName);
            this.fmeCustomerData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fmeCustomerData.Location = new System.Drawing.Point(134, 12);
            this.fmeCustomerData.Name = "fmeCustomerData";
            this.fmeCustomerData.Size = new System.Drawing.Size(405, 132);
            this.fmeCustomerData.TabIndex = 1;
            this.fmeCustomerData.TabStop = false;
            this.fmeCustomerData.Text = "Datos Del Cliente";
            // 
            // txtBenefID
            // 
            this.txtBenefID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBenefID.Enabled = false;
            this.txtBenefID.Location = new System.Drawing.Point(149, 102);
            this.txtBenefID.Name = "txtBenefID";
            this.txtBenefID.Size = new System.Drawing.Size(250, 22);
            this.txtBenefID.TabIndex = 7;
            // 
            // txtCustID
            // 
            this.txtCustID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCustID.Enabled = false;
            this.txtCustID.Location = new System.Drawing.Point(149, 78);
            this.txtCustID.Name = "txtCustID";
            this.txtCustID.Size = new System.Drawing.Size(250, 22);
            this.txtCustID.TabIndex = 5;
            // 
            // txtAddress
            // 
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.Enabled = false;
            this.txtAddress.Location = new System.Drawing.Point(149, 53);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(250, 22);
            this.txtAddress.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Enabled = false;
            this.txtName.Location = new System.Drawing.Point(149, 28);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(250, 22);
            this.txtName.TabIndex = 1;
            // 
            // lblBenefID
            // 
            this.lblBenefID.AutoSize = true;
            this.lblBenefID.Enabled = false;
            this.lblBenefID.Location = new System.Drawing.Point(20, 105);
            this.lblBenefID.Name = "lblBenefID";
            this.lblBenefID.Size = new System.Drawing.Size(100, 13);
            this.lblBenefID.TabIndex = 6;
            this.lblBenefID.Text = "Iden. Beneficiario:";
            // 
            // lblCustID
            // 
            this.lblCustID.AutoSize = true;
            this.lblCustID.Enabled = false;
            this.lblCustID.Location = new System.Drawing.Point(20, 81);
            this.lblCustID.Name = "lblCustID";
            this.lblCustID.Size = new System.Drawing.Size(113, 13);
            this.lblCustID.TabIndex = 4;
            this.lblCustID.Text = "Iden. Contribuyente:";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Enabled = false;
            this.lblAddress.Location = new System.Drawing.Point(20, 56);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(58, 13);
            this.lblAddress.TabIndex = 2;
            this.lblAddress.Text = "Dirección:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Enabled = false;
            this.lblName.Location = new System.Drawing.Point(20, 31);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(51, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Nombre:";
            // 
            // RequestFiscalDataForm
            // 
            this.AcceptButton = this.cmdOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdCancel;
            this.ClientSize = new System.Drawing.Size(547, 180);
            this.ControlBox = false;
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.fmeCustomerData);
            this.Controls.Add(this.fmeCustomerType);
            this.Controls.Add(this.cmdOK);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "RequestFiscalDataForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.fmeCustomerType.ResumeLayout(false);
            this.fmeCustomerType.PerformLayout();
            this.fmeCustomerData.ResumeLayout(false);
            this.fmeCustomerData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.GroupBox fmeCustomerType;
        private System.Windows.Forms.RadioButton optCustOrg;
        private System.Windows.Forms.RadioButton optCustSenior;
        private System.Windows.Forms.RadioButton optCustDisabled;
        private System.Windows.Forms.RadioButton optCustNormal;
        private System.Windows.Forms.GroupBox fmeCustomerData;
        private System.Windows.Forms.TextBox txtBenefID;
        private System.Windows.Forms.TextBox txtCustID;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblBenefID;
        private System.Windows.Forms.Label lblCustID;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblName;
    }
}