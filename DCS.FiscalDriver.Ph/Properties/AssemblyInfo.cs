﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DCS.FiscalDriver.Ph")]
[assembly: AssemblyDescription("Philippinean fiscal printer driver for Sacoa POS and Kiosk.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Sacoa Playcard")]
[assembly: AssemblyProduct("DCS.FiscalDriver.Ph")]
[assembly: AssemblyCopyright("Copyright © ML 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("599cb22b-3f49-4875-b0c6-0df7e957dc30")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.1.10")]
[assembly: AssemblyFileVersion("1.0.1.10")]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "app.config")]
//[assembly: log4net.Config.XmlConfigurator(Watch = true)]

//1.0.0.0   TODO: Modifiers' taxes are assumed to be the same as Products' taxes. This may not be desirable.
//1.0.0.1   Referencio WindowsPrinter v1.0.1.1
//1.0.1.9   ConfigEngine seems to do weird sh!t "sometimes", so it is replaced by old, tried-and-true, INI 
//          reading functions taken almost directly from Kiosk v15 FrontEnd's modUtils.
//1.0.1.10  (20220905, ML) Updated PerformZRead.Sql in the following way:	
//            --1.0.1.10 20220905 ML: Use ReturnsTotal as positive for further algebraic sums.
//            --, ISNULL(SUM(ReturnsTotal), 0) AS ReturnsTotal	
//            , ISNULL(SUM(ABS(ReturnsTotal)), 0) AS ReturnsTotal
//          The idea is to have ReturnsTotal always positive, and at the same time be able to use old data
//          without giving wrong values. The algebraic sum later where "ReturnsTotal" is subtracted results
//          in the subtraction of a negative number, yielding a positive value.