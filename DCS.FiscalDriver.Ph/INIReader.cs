﻿public static class INIReader
{

    [System.Runtime.InteropServices.DllImport("Kernel32.Dll",
        EntryPoint = "GetPrivateProfileStringW",
        SetLastError = true,
        CharSet = System.Runtime.InteropServices.CharSet.Unicode,
        ExactSpelling = true,
        CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)
    ]
    private static extern int GetPrivateProfileString(string section,
                                                      string key,
                                                      string defaultValue,
                                                      System.Text.StringBuilder valueData,
                                                      int bytesRead,
                                                      string iniFileName
                                                     );

    [System.Runtime.InteropServices.DllImport("Kernel32.Dll",
        EntryPoint = "WritePrivateProfileStringW",
        SetLastError = true,
        CharSet = System.Runtime.InteropServices.CharSet.Unicode,
        ExactSpelling = true,
        CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)
    ]
    private static extern int WritePrivateProfileString(string section,
                                                        string key,
                                                        string valueData,
                                                        string iniFileName
                                                       );

    public static bool INIDeleteKey(string section, 
                                      string key, 
                                      string iniFileName
                                     )
    {

        int retCode = WritePrivateProfileString(section, 
                                                key, 
                                                null, 
                                                iniFileName
                                               );

        return (retCode != 0);

    }

    public static string INIGet(string section, 
                                  string key, 
                                  string defaultValue, 
                                  string iniFileName
                                 )
    {

        System.Text.StringBuilder buffer = new System.Text.StringBuilder('\0', 1<<10);
        int bufferLen = buffer.Length - 1;
        int retCode = GetPrivateProfileString(section,
                                              key, 
                                              defaultValue, 
                                              buffer,
                                              bufferLen,
                                              iniFileName
                                             );

        return (buffer.ToString().Substring(0, retCode));

    }

    //public static string[] INIGetSectionKeys(string section,
    //                                           string iniFileName
    //                                          )
    //{

    //    string terminator = "\0\0";
    //    string buffer = new string('\0', 1 << 16);
    //    int bufferLen = buffer.Length - 1;
    //    int retCode = GetPrivateProfileSectionKeys(section,
    //                                               ref buffer,
    //                                               bufferLen,
    //                                               iniFileName
    //                                              );
    //    string retVal = "";

    //    if (buffer.IndexOf(terminator) > 0)
    //    {
    //        retVal = buffer.Substring(0, buffer.IndexOf(terminator));
    //    }

    //    return (retVal.Split('\0'));

    //}

}