﻿DECLARE @LastInvoiceNumber BIGINT
DECLARE @LastReadTotalGross DECIMAL(18, 4)
DECLARE @LastZReadId INT
DECLARE @ComputerID INT
DECLARE @FirstInvoiceId BIGINT
DECLARE @LastInvoiceId BIGINT

--DECLARE @ComputerName NVARCHAR(16)
--SET @ComputerName = 'PHATBASTARD'
SET @ComputerID = (SELECT ID FROM datStoreComputers WHERE ComputerName = @ComputerName)

SELECT 
	@LastInvoiceNumber = ISNULL(MAX(LastInvoiceNumber), 0),	
	@LastZReadId = MAX(ZReadId)
FROM Philippines_ZReads
WHERE (ComputerName = @ComputerName)

IF @LastZReadId IS NOT NULL 
	BEGIN
		SELECT 
			--@LastInvoiceNumber = LastInvoiceNumber, 
			--@LastReadTotalGross = TotalGross --NOOOOOOO!!! Must use the historically accumulated value, not "yesterday's total"!!!
			@LastReadTotalGross = ClosingSalesReading
		FROM Philippines_ZReads
		WHERE ((ZReadId = @LastZReadID) AND
		       (ComputerName = @ComputerName)
			  )
	END
ELSE 
	BEGIN
		SELECT @LastInvoiceNumber = 0
		SELECT @LastReadTotalGross = 0
	END

BEGIN TRANSACTION

INSERT INTO Philippines_ZReads
	(ReadDate
	, ComputerName
	, FirstInvoiceNumber
	, LastInvoiceNumber
	, OpeningSalesReading
	, ClosingSalesReading
	, TotalGross
	, SalesReturns
	, SalesDiscounts
	, VatOfExemptSales
	, NetSales
	, VatableSales
	, VatExemptSales
	, VatAmount
	, ZeroRatedSales
	, TotalNetSales)
SELECT
	GetDate()
	, @ComputerName AS ComputerName
	--, ISNULL(MIN(InvoiceNumber), @LastInvoiceNumber) AS FirstInvoiceNumber
	, ISNULL(MIN(InvoiceNumber), 0) AS FirstInvoiceNumber
	, ISNULL(MAX(InvoiceNumber), 0) AS LastInvoiceNumber
	--, ISNULL(@LastReadTotalGross, 0) AS LastReadTotalGross
	, @LastReadTotalGross AS OpeningSalesReading
	, ISNULL(SUM(SalesTotal), 0) + ISNULL(@LastReadTotalGross, 0) AS ClosingSalesReading
	, ISNULL(SUM(SalesTotal), 0) AS SalesTotal
	-- 1.0.1.10 20220905 ML: Use ReturnsTotal as positive for further algebraic sums.
	--, ISNULL(SUM(ReturnsTotal), 0) AS ReturnsTotal
	, ISNULL(SUM(ABS(ReturnsTotal)), 0) AS ReturnsTotal
	, ISNULL(SUM(SalesDiscount), 0) AS DiscountsTotal
	, ISNULL(SUM(VatOfExemptSales), 0) AS VatOfExemptSales
	-- 1.0.1.10 20220905 ML: Make sure to subtract positive values in order to get proper amounts.
	--, ISNULL(SUM(SalesTotal - ReturnsTotal - SalesDiscount - VatOfExemptSales), 0) AS NetSales --Should match TotalNetSales
	, ISNULL(SUM(SalesTotal - ABS(ReturnsTotal) - ABS(SalesDiscount) - ABS(VatOfExemptSales)), 0) AS NetSales --Should match TotalNetSales
	, ISNULL(SUM(VatableSales), 0) AS VatableSales
	, ISNULL(SUM(VatExemptSales), 0) AS VatExemptSales
	, ISNULL(SUM(VatTotal), 0) AS VatAmount
	, ISNULL(SUM(ZeroRatedSales), 0) AS ZeroRatedSales
	, ISNULL(SUM(VatableSales + VatTotal + VatExemptSales + ZeroRatedSales), 0) AS TotalNetSales -- Should match NetSales
	
FROM Philippines_Invoices 
WHERE ((InvoiceNumber > @LastInvoiceNumber) AND 
       (MachineID = @ComputerID)
	  )

DECLARE @InsertedId BIGINT
SELECT @InsertedId = SCOPE_IDENTITY()

--ADD PAYMENT DETAIL
INSERT INTO 
	Philippines_ZReads_PaymentDetail
    (ZReadId, 
	 CurrencyName, 
	 TotalAmount
	)
	SELECT
		@InsertedId,
		CurrencyName,
		SUM(TotalAmount)
	FROM
		Philippines_Invoices_PaymentDetail
		INNER JOIN 
			Philippines_Invoices
		ON
			Philippines_Invoices.InvoiceId = Philippines_Invoices_PaymentDetail.InvoiceId
	WHERE ((Philippines_Invoices.InvoiceNumber > @LastInvoiceNumber) AND 
	       (Philippines_Invoices.MachineID = @ComputerID)
		  )
    GROUP BY CurrencyName

SELECT 
	ZReadId,
	ReadDate,
	ComputerName,
	FirstInvoiceNumber,
	LastInvoiceNumber,
	OpeningSalesReading,
	ClosingSalesReading,
	TotalGross AS SalesTotal,
	SalesReturns AS ReturnsTotal,
	SalesDiscounts AS DiscountsTotal,
	VatOfExemptSales,
	NetSales,
	VatableSales,
	VatExemptSales,
	VatAmount,
	ZeroRatedSales,		
	TotalNetSales
FROM 
	Philippines_ZReads
WHERE
	ZReadId = @InsertedId

SELECT 
	CurrencyName, 
	TotalAmount
FROM
	Philippines_ZReads_PaymentDetail
WHERE
	ZReadId = @InsertedId

COMMIT TRANSACTION