﻿/*
 * This code saves a transaction. Will return -1 if could not save, or a positive non-zero
 * value if OK.
 * Inputs:
 *   @ComputerName
 *   @OpDate
 *   @OpRegNumber
 *   @OpInvoiceNumber
 *   @OpCashierTransactionID
 *   @OpCustomerName
 *   @OpCustomerAddress
 *   @OpInvoiceID
 *   @OpSalesTotal
 *   @OpReturnsTotal
 *   @OpSalesDiscount
 *   @OpZeroRatedSales
 *   @OpVATableSales
 *   @OpVATExemptSales
 *   @OpVATTotal
 *   @OpVATOfExemptSales
 *   @OpInvoiceType
 * Outputs:
 *   @InsertedID: The autonumber record ID just inserted, or -1 if error.
 */


DECLARE @InsertedId BIGINT
DECLARE @ComputerID INT

--Assume ERROR
SET @InsertedID = -1	

BEGIN TRANSACTION

	SET @ComputerID = (SELECT ID FROM datStoreComputers WHERE ComputerName = @ComputerName)

	INSERT INTO Philippines_Invoices
		(MachineID,
		 TransDate, 
		 RegistrationNumber, 
		 InvoiceNumber, 
		 CashierTransactionId, 
		 CustomerName, 
		 CustomerAddress, 
		 ScPwdRegistrationId, 
		 SalesTotal, 
		 ReturnsTotal,
		 SalesDiscount,      
		 ZeroRatedSales,
		 VatableSales,
		 VatExemptSales,
		 VatTotal, 
		 VatOfExemptSales,
		 InvoiceType
		)	
	VALUES
		(@ComputerID,
		 @OpDate, 
		 @OpRegNumber, 
		 @OpInvoiceNumber, 
		 @OpCashierTransactionID, 
		 @OpCustomerName, 
		 @OpCustomerAddress, 
		 @OpInvoiceID,
		 @OpSalesTotal, 
		 @OpReturnsTotal,
		 @OpSalesDiscount, 
		 @OpZeroRatedSales,
		 @OpVATableSales, 
		 @OpVATExemptSales, 
		 @OpVATTotal, 
		 @OpVATOfExemptSales, 
		 @OpInvoiceType
		)

	SET @InsertedId = SCOPE_IDENTITY()

COMMIT TRANSACTION

SELECT @InsertedID AS InsertedID