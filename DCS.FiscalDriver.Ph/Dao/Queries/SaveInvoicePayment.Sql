﻿/*
 * This code saves a payment made for a transaction. Must be called once per payment,
 * right after successfully saving an invoice. It needs the InvoiceID to create the 
 * master-detail relationship. Will return -1 if could not save, or a positive non-zero
 * value if OK.
 * Inputs:
 *   @InvoiceID: The ID just inserted when saving the invoice.
 *   @TenderName: The currency name.
 *   @AmountPaid: The amount paid (not actually tendered, as one may pay a $1.5 transaction with $2, where $1.5 is PaidAmount and $2 is TenderedAmont).
 * Outputs:
 *   @InsertedID: The autonumber record ID just inserted, or -1 if error.
 */

DECLARE @PaymentID INT

-- Assume error
SET @PaymentID = -1

BEGIN TRANSACTION

	INSERT INTO Philippines_Invoices_PaymentDetail
		( InvoiceID
		, CurrencyName
		, TotalAmount
		)
	VALUES
		( @InvoiceID
		, @CurrencyName
		, @AmountPaid
		)

	SET @PaymentID = SCOPE_IDENTITY()

COMMIT TRANSACTION

SELECT @PaymentID AS PaymentID