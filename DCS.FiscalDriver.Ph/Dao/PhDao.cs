﻿using System;
using System.Data.SqlClient;
using DCS.FiscalDriver.Ph.Domain;

namespace DCS.FiscalDriver.Ph.Dao
{
	public class PhDao
	{
		private readonly string _connString;

		public PhDao()
		{
			string scardIniFile = System.IO.Path.Combine(Environment.GetEnvironmentVariable("NW"), "Data\\SCard.Ini");
			ConfigEngine.Engine config = new ConfigEngine.Engine("POS", "PrtDrv_FcalPh.cfg");

			_connString = config.GetItem("SQL", "DotNET_ConnectionString", "", scardIniFile);

			Encrypt.clsEncrypt encrypt = new Encrypt.clsEncrypt();
			string user = encrypt.DecryptHard(config.GetItem("SQL", "User", "", scardIniFile), "PAMPITA");
			string password = encrypt.DecryptHard(config.GetItem("SQL", "Password", "", scardIniFile), "PAMPITA");

			_connString = string.Concat(_connString, $";user={user};password={password}");

			//Test Connection
			using (var conn = new SqlConnection(_connString))
			{
				try
				{

                    conn.Open();
                    CheckTablesExist(conn);

                }
                    catch (SqlException e)
				{
					throw new ApplicationException(
						$"Database connection test failed for connection string [{_connString}]: {e.ToString()}");
				}
			}

		}

        void CheckTablesExist(SqlConnection conn)
        {

            if(!CheckTableExist(conn, "Philippines_Invoices"))
            {
                throw new ApplicationException(
                    "SQL table 'Philippines_Invoices' does not exist, migration v12.6 needed.");
            }

            if (!CheckTableExist(conn, "Philippines_Invoices_PaymentDetail"))
            {
                throw new ApplicationException(
                    "SQL table 'Philippines_Invoices_PaymentDetail' does not exist, migration v12.6 needed.");
            }

            if (!CheckTableExist(conn, "Philippines_ZReads"))
            {
                throw new ApplicationException(
                    "SQL table 'Philippines_ZReads' does not exist, migration v12.6 needed.");
            }

            if (!CheckTableExist(conn, "Philippines_ZReads_PaymentDetail"))
            {
                throw new ApplicationException(
                    "SQL table 'Philippines_ZReads_PaymentDetail' does not exist, migration v12.6 needed.");
            }

        }

        bool CheckTableExist(SqlConnection conn, string tableName)
        {

            var retVal = false;
            var query = $@"
IF OBJECT_ID('{tableName}') IS NULL
    BEGIN
        SELECT 0 
    END 
ELSE
    BEGIN
        SELECT 1
    END";

            using (var checker = new SqlCommand(query, conn))
            {
                retVal = ((int)checker.ExecuteScalar() == 1);
            }

            return retVal;

        }

		public bool SaveInvoice(
			 SalesInvoice invoiceData
			)
		{

			//Assume failure.
			long insertedId = -1;

			//Fetch computername for SP.
			var computerName = System.Environment.GetEnvironmentVariable("ComputerName");

			using (var conn = new SqlConnection(_connString))
			{

				conn.Open();
				var trn = conn.BeginTransaction();
				using (
					var cmd = new SqlCommand(EmbeddedResource.GetString("Dao.Queries.SaveInvoice.Sql"),
						conn, trn)
				)
				{

					cmd.Parameters.AddWithValue("@ComputerName", computerName);
					cmd.Parameters.AddWithValue("@OpDate", invoiceData.OperationDate);
					cmd.Parameters.AddWithValue("@OpRegNumber", invoiceData.RegistrationNumber);
					cmd.Parameters.AddWithValue("@OpInvoiceNumber", invoiceData.InvoiceNumber);
					cmd.Parameters.AddWithValue("@OpCashierTransactionID", invoiceData.CashierTransactionId);
					cmd.Parameters.AddWithValue("@OpCustomerName", invoiceData.CustomerName);
					cmd.Parameters.AddWithValue("@OpCustomerAddress", invoiceData.CustomerAddress);
					cmd.Parameters.AddWithValue("@OpInvoiceID", invoiceData.InvoiceId);
					cmd.Parameters.AddWithValue("@OpSalesTotal", invoiceData.SalesTotal);
					cmd.Parameters.AddWithValue("@OpReturnsTotal", invoiceData.ReturnsTotal);
					cmd.Parameters.AddWithValue("@OpSalesDiscount", invoiceData.SalesDiscount);
					cmd.Parameters.AddWithValue("@OpZeroRatedSales", invoiceData.ZeroRatedSales);
					cmd.Parameters.AddWithValue("@OpVATableSales", invoiceData.VatableSales);
					cmd.Parameters.AddWithValue("@OpVATExemptSales", invoiceData.VatExemptSales);
					cmd.Parameters.AddWithValue("@OpVATTotal", invoiceData.VatTotal);
					cmd.Parameters.AddWithValue("@OpVATOfExemptSales", invoiceData.VatOfExemptSales);
					cmd.Parameters.AddWithValue("@OpInvoiceType", (int)invoiceData.InvoiceType);

					insertedId = (long)cmd.ExecuteScalar();
				}
				if (insertedId == -1)
				{
					trn.Rollback();
					return false;

				}
				foreach (var pmt in invoiceData.Payments)
				{

					using (
							 var sqlCommand = new SqlCommand(EmbeddedResource.GetString("Dao.Queries.SaveInvoicePayment.Sql"),
							  conn, trn)
							  )
					{
						sqlCommand.Parameters.AddWithValue("@InvoiceID", insertedId);
						sqlCommand.Parameters.AddWithValue("@CurrencyName", pmt.CurrencyName);
						//Yup, misnomer here! We want to know how much of the transaction the customer PAID, not the
						//amount actually TENDERED for paying. If a $7 transaction is paid with $10, the AmountPaid is
						//$7, while the AmountTendered is $10. A non-existing ChangeBackAmount would be $3. Got it?
						//Extend this concept to multiple payments per transaction and we have a nice name-salad.
						//=> Fixed
						sqlCommand.Parameters.AddWithValue("@AmountPaid", pmt.AmountPaid);
						sqlCommand.ExecuteNonQuery();
					}

				}
				trn.Commit();
				return true;

			}
		}

		public Zread PerformZRead()
		{
			var computerName = System.Environment.GetEnvironmentVariable("ComputerName");

			using (var conn = new SqlConnection(_connString))
			{
				conn.Open();
				using (
					var cmd = new SqlCommand(EmbeddedResource.GetString("Dao.Queries.PerformZRead.sql"),
					conn)
					)
				{

					cmd.Parameters.AddWithValue("@ComputerName", computerName);

					var reader = cmd.ExecuteReader();					
					if (!reader.Read())
					{
						throw (new ApplicationException("PerformZRead produced no result"));
					}
					
					var zRead = new Zread();

					zRead.ComputerName = (string)reader["ComputerName"];
					zRead.ZreadDate = (DateTime)reader["ReadDate"];
					zRead.ZreadId = (int)reader["ZReadId"];
					zRead.OpeningSalesReading = (decimal)reader["OpeningSalesReading"];
					zRead.ClosingSalesReading = (decimal)reader["ClosingSalesReading"];
					zRead.FirstInvoiceNumber = (long)reader["FirstInvoiceNumber"];
					zRead.LastInvoiceNumber = (long)reader["LastInvoiceNumber"];
					zRead.SalesTotal = (decimal)reader["SalesTotal"];
					zRead.ReturnsTotal = (decimal)reader["ReturnsTotal"];
					zRead.DiscountsTotal = (decimal)reader["DiscountsTotal"];
					zRead.VatOfExemptSales = (decimal)reader["VatOfExemptSales"];
					zRead.VatAmount = (decimal)reader["VatAmount"];
					zRead.VatableSales = (decimal)reader["VatableSales"];
					zRead.ZeroRatedSales = (decimal)reader["ZeroRatedSales"];
					zRead.VatExemptSales = (decimal)reader["VatExemptSales"];
					zRead.TotalNetSales = (decimal)reader["TotalNetSales"];
					zRead.NetSales = (decimal)reader["NetSales"];

					if (reader.NextResult())
					{
						while (reader.Read())
						{
                            //var payment = new ZReadPaymentItem((string) reader["CurrencyName"], (decimal) reader["TotalAMount"]);
                            //zRead.Payments.Add(payment);
                            zRead.Payments.Add(new ZReadPaymentItem((string)reader["CurrencyName"], 
                                                                    (decimal)reader["TotalAMount"]
                                                                   )
                                              );
                        }

					}

                    return (zRead);
					
				}
			}
		}
  
	}

}
