﻿namespace DCS.FiscalDriver.Ph
{
	public class Transaction
	{
        public InvoiceCustomerData InvoiceCustomerData { get; }

        public Transaction(InvoiceCustomerData invoiceCustomerData)
		{
			InvoiceCustomerData = invoiceCustomerData;
		}

	}
}
