﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCS.FiscalDriver.Ph
{
    public class InvoiceCustomerData
    {

		//public enum TiposDeClienteEnum
		//{
		//	ConsumidorFinal = 1,
		//	Sc = 2,
		//	Pwd = 3,
		//	Peza = 4
		//}

        public Domain.SalesInvoice.InvoiceTypesEnum CustomerType { get; set; } = Domain.SalesInvoice.InvoiceTypesEnum.Normal;

        public string CustomerName { get; set; } = "";

        public string CustomerAddress { get; set; } = "";

        public string CustomerDocNumber { get; set; } = "";

        public string CustomerBeneficiaryNumber { get; set; } = "";

        public bool AutorizadoCliente { get; set; }

    }
}
