﻿using DCS.FiscalDriver.Ph.Dao;
using DCS.FiscalDriver.Ph.Domain;
using System;
using System.Collections.Generic;
using System.IO;

namespace DCS.FiscalDriver.Ph
{
    public class Driver: DCS.FiscalDriverBase.IFiscalDriverBase
    {

        /*
         * VERSION INFO
         * 
         * 1.0.1.8: References updated to use DCS.FiscalDriverBase v1.0.2.2.
         * 
         * 1.0.1.7: Can force NORMAL-ONLY invoicing by INI for Kiosks.
         * 
         * 1.0.1.6: comment lost
         * 
         * 1.0.1.5: comment lost
         * 
         * 1.0.1.4: comment lost
         * 
         * 1.0.1.3: Logs version to log upon startup.
         * 
         * 1.0.1.2: Changes in tax handling, basically it ignores the transaction 
         *          tax data sent in non "Normal" transactions.
         * 
         * 1.0.1.1: Changes the way taxes are handled and shown in order to fill
         *          the templates with good data. Beware: Must use POS v3.01 and
         *          POSEngine v2.3.1.5 as a minimum.
         * 
         * 1.0.1.0: Removes ActiveX dependencies (except Encrypt), must be called 
         *          in POS via the PrtDrv_FcalInvoicing bridge. Changed reference 
         *          to newer DCS.FiscalPrinterBase which has no ActiveX deps.
         *          All GUIDs removed so the project is not callable from COM.
         * 
         * 1.0.0.1: Initial release - Some aspect need to be modified. Uses 
         *          older DCS.FiscalPrinterBase that depend upon ActiveX objects.
         * 
         */

        private const string        NO_PRINT = "";
        private const string        MODULE_CONFIG_FILE = "DCS.FiscalDriver.Ph.Dll.Config";

        private int                 _lastError = 0;
	    private string              _lastErrorString = "";
	    private string              _sysFolder = "";
		private string              _normalPrintTemplate = "";
		private string              _scpwdPrintTemplate = "";
        private string              _orgPrintTemplate = "";
        private string              _nonfiscalPrintTemplate = "";
        private string              _zReadPrintTemplate = "";
		private int                 _posRegistrationNumber = 0;
        private string              _currencyFormatString = "N2";
		private int                 _discountId = 0;	    
	    private log4net.ILog        _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
	    private Transaction         _currentTransaction = null;
        private InvoiceCustomerData _invoiceCustomerData = null;
        private string              _cardProductName = "Card ";
        private string              _nullCardProductName = "Products";
        private string              _driverConnectionString = "";
        private bool                _useOnlyNormalInvoicing = false;

		private PhDao _dao;

		public Driver()
		{			
			//Constructor must be empty for COM-exposed classes.
		}

        private void ClearLastError()
        {
            _lastErrorString = string.Empty;
            _lastError = 0;
        }

        bool FiscalDriverBase.IFiscalDriverBase.Create(string drive, string dummy)
	    {

            // Chequear existencia, leer y verificar validez de variable de entorno NW
            // Inicializar logueo
            // Hacer lo mismo con los INIs y otros parametros de configuracion de este driver.
            // Ejemplos serían distintos porcentajes de TAX, descuentos especiales para Jubilados o discapacitados, configuracion del hardware de impresora (comport)
            // Inicializar hardware (impresora) y verificar que esté online
            // Logear detalladamente todo lo que se hace

            ClearLastError();

            try
            {
                //Initialize log4net logging
                log4net.Config.XmlConfigurator.Configure(new FileInfo(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), MODULE_CONFIG_FILE)));
            }
            catch(Exception ex)
            {
                _lastError = 513;
                _lastErrorString = $"Error while starting (configuring) log4net: {ex.ToString()}";
                return false;
            }

            _log.Info($"Initializing Philippines Fiscal Driver v{GetVersion()}");
            if (drive != "")
            {
                _sysFolder = drive;
            }
            else
            {
                _sysFolder = Environment.GetEnvironmentVariable("NW");

				if (_sysFolder == "")
			    {
                    _lastError = 513;
                    _lastErrorString = "Unable to determine system drive";
				    return false;
			    }			    
			}

		    if (!Directory.Exists(_sysFolder))
		    {
                _lastError = 513;
                _lastErrorString = $"Invalid system folder: {_sysFolder}";
			    return false;
		    }

		    string errorString;
		    if (!GetConfig(out errorString))
		    {
                _lastError = 513;
                _lastErrorString = $"Error reading configuration: [{errorString}]";
				return false;
		    }

			try
			{ 
			    _dao = new PhDao();
			}
			catch(Exception e)
			{
                _lastError = 513;
                _lastErrorString = $"Error initializing Phillipines DAO: {e.ToString()}";
				return false;
			}

            return true;

	    }

        private bool GetConfig(out string errorString)
        {

            string setting;
            //string iniFilename = _sysFolder + "\\DCS\\Config\\POS\\PrtDrv_FcalPh.Cfg";
            ConfigEngine.Engine config = new ConfigEngine.Engine("POS", "PrtDrv_FcalPh.Cfg");
            

            ClearLastError();
            errorString = "";

            setting = config.GetItem(Environment.MachineName, "Printer Connection String");
            if (string.IsNullOrEmpty(setting))
            {
                errorString = $"Missing [{Environment.MachineName}]Printer Connection String= setting";
                return false;
            }
            _driverConnectionString = setting;

            setting = config.GetItem("General", "Discount Id");
            if (!int.TryParse(setting, out _discountId))
            {
				errorString = $"Missing or invalid [General]Discount Id= setting: '{setting}'";
				return false;
            }

            setting = config.GetItem(Environment.MachineName, "POS Registration Number");
            if (!int.TryParse(setting, out _posRegistrationNumber))
            {
                errorString = $"Missing or invalid [{Environment.MachineName}]POS Registration Number= setting: '{setting}'";
                return false;
            }

            setting = config.GetItem(Environment.MachineName, "Force Normal Invoicing Only", "False");
            if (!bool.TryParse(setting, out _useOnlyNormalInvoicing))
            {
                //1.0.1.7: New setting for Kiosks, non-fatal if missing.
                errorString = $"Missing or invalid [{Environment.MachineName}]Force Normal Invoicing Only= setting: '{setting}', defaulting to FALSE.";
                _useOnlyNormalInvoicing = false;
            }

            _cardProductName = config.GetItem("International", "Card Product Description", _cardProductName);
            _nullCardProductName = config.GetItem("International", "Null Card Product Description", _nullCardProductName);

            _currencyFormatString = config.GetItem("General", "Currency Format String", _currencyFormatString); //Defaults to N2, numeric with 2 decimals.
            
			_normalPrintTemplate = Path.Combine(_sysFolder, $@"DCS\Config\POS\PrtDrv_FcalPh_{Environment.MachineName}_NormalTemplate.Prn");
	        _scpwdPrintTemplate = Path.Combine(_sysFolder, $@"DCS\Config\POS\PrtDrv_FcalPh_{Environment.MachineName}_ScPwdTemplate.Prn");
            _orgPrintTemplate = Path.Combine(_sysFolder, $@"DCS\Config\POS\PrtDrv_FcalPh_{Environment.MachineName}_OrgTemplate.Prn");
            _zReadPrintTemplate = Path.Combine(_sysFolder, $@"DCS\Config\POS\PrtDrv_FcalPh_{Environment.MachineName}_ZReadTemplate.Prn");
            _nonfiscalPrintTemplate = Path.Combine(_sysFolder, $@"DCS\Config\POS\PrtDrv_FcalPh_{Environment.MachineName}_NonFiscalTemplate.Prn");

            if (!File.Exists(_normalPrintTemplate))
            {
                errorString = $"Missing normal ticket print template: [{_normalPrintTemplate}]";
                return false;
            }

            if (!File.Exists(_scpwdPrintTemplate))
            {
                errorString = $"Missing SC/PWD ticket print template: [{_scpwdPrintTemplate}]";
                return false;
            }

            if (!File.Exists(_orgPrintTemplate))
            {
                errorString = $"Missing organizational ticket print template: [{_orgPrintTemplate}]";
                return false;
            }

            if (!File.Exists(_zReadPrintTemplate))
            {
                errorString = $"Missing Z-Read ticket print template: [{_zReadPrintTemplate}]";
                return false;
            }

            if (!File.Exists(_nonfiscalPrintTemplate))
            {
                errorString = $"Missing Non-Fiscal voucher print template: [{_nonfiscalPrintTemplate}]";
                return false;
            }

            return true;

        }

        bool FiscalDriverBase.IFiscalDriverBase.DriverPrintsFromTransactionObject()
        {
            ClearLastError();
            return true; //Devuelve true porque esta impresora es ADEMAS un controlador fiscal
        }

        int FiscalDriverBase.IFiscalDriverBase.LastError()
        {
            return _lastError;
        }

        string FiscalDriverBase.IFiscalDriverBase.LastErrorString()
        {
            return _lastErrorString;
        }

        bool FiscalDriverBase.IFiscalDriverBase.NeedsZClose()
	    {
            //A pesar del nombre, esta rutina lo que hace es devolver 'FALSE' si la transacción puede continuar o 'TRUE' si no puede
            //En un principio la razon por la que no podía proseguir la transacción era porque hacía falta hacer un cierre Z por haber cambiado el día fiscal
            //Pero pueden existir otras razones, como un certificado/habilitación vencida/expirada.

            ClearLastError();
            return false;
        }
		
	    bool PrintZRead(Zread zRead)
	    {

            /*
             * Implements the new DCS.Driver.SacoaPrinters.XXXXX interfaces.
             */

            var retVal = false;

            ClearLastError();

            try
            {

                //Create driver and data dictionary
                var driver = new Drivers.SacoaPrinters.WindowsPrinter.Driver(_driverConnectionString);
                var xactDict = new DCS.Drivers.SacoaPrinters.SacoaPrinterBase.TicketData();

                //Fill data dictionary
                xactDict.AddToDictionary("ZRD_DATE", zRead.ZreadDate.ToString("MMM-dd-yyyy"));
                xactDict.AddToDictionary("ZRD_TIME", zRead.ZreadDate.ToString("HH:mm"));
                xactDict.AddToDictionary("ZRD_POS", Environment.GetEnvironmentVariable("ComputerName").ToString());
                xactDict.AddToDictionary("ZRD_POSREGNUMBER", _posRegistrationNumber.ToString());

                /*
                 * Properties ZRD_CASHIER* may have to be implemented.
                 * For the time being these are not available, and we suspect
                 * this is more an inkshitter requirement than a lawful one.
                 */
                //xactDict.AddToDictionary("ZRD_OPERATORID", "");
                //xactDict.AddToDictionary("ZRD_OPERATORFIRST", "");
                //xactDict.AddToDictionary("ZRD_OPERATORLAST", "");
                //xactDict.AddToDictionary("ZRD_OPERATORNICK", "");

                xactDict.AddToDictionary("ZRD_INVOICEFIRST", zRead.FirstInvoiceNumber.ToString());
                xactDict.AddToDictionary("ZRD_INVOICELAST", zRead.LastInvoiceNumber.ToString());
                xactDict.AddToDictionary("ZRD_OPENINGGROSSTOTAL", zRead.OpeningSalesReading.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_CLOSINGGROSSTOTAL", zRead.ClosingSalesReading.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_ZREADNUMBER", zRead.ZreadId.ToString());

                xactDict.AddToDictionary("ZRD_TOTALGROSSSALES", zRead.SalesTotal.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_SALESRETURNS", zRead.ReturnsTotal.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_SALESDISCOUNTS", zRead.DiscountsTotal.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_VATOFEXEMPTSALES", zRead.VatOfExemptSales.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_NETSALES", zRead.NetSales.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_VATABLESALES", zRead.VatableSales.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_VATAMOUNT", zRead.VatAmount.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_VATEXEMPTSALES", zRead.VatExemptSales.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_ZERORATEDSALES", zRead.ZeroRatedSales.ToString(_currencyFormatString));
                xactDict.AddToDictionary("ZRD_TOTALNETSALES", zRead.TotalNetSales.ToString(_currencyFormatString));

                //Add payment summary as a repeat block (again smells like an inkshitter wish)
                DCS.Drivers.SacoaPrinters.SacoaPrinterBase.RepeatBlockData pmtsBlk = xactDict.AddRepeatBlock("Payments");
                foreach(ZReadPaymentItem pmt in zRead.Payments)
                {
                    DCS.Drivers.SacoaPrinters.SacoaPrinterBase.RepeatBlockDataItem pmntItem = pmtsBlk.AddNewItem();
                    pmntItem.AddToDictionary("ZRD_PAYMENTDESCRIPTION", pmt.CurrencyName);
                    pmntItem.AddToDictionary("ZRD_PAYMENTAMOUNT", pmt.TotalAmount.ToString(_currencyFormatString));
                }

                //Finally, print the report
                driver.PrintTicket(_zReadPrintTemplate, xactDict);

                retVal = true;

            }
            catch(Exception ex)
            {

                _lastErrorString = $"Error printing Z-Read: {ex.Message}";
                _log.Error($"Error printing Z-Read: {ex.ToString()}");
                retVal = false;

            }

            return retVal;

        }

        bool FiscalDriverBase.IFiscalDriverBase.PerformZClose(
            ref int zCloseNumber, 
            bool dummyPrintHeaderAndFooter)
	    {

            /* Must perform and print what philippineans call a Z-Read.
             * A Z-Read is the consolidation (sum) of all totals since the 
             * last Z-Read to the moment the Z-Read is invoked.
             */

            Zread zRead;
                        
			ClearLastError();

            try
            {
                zRead = _dao.PerformZRead();
            }
            catch (Exception ex)
            {
                _lastErrorString = $"Error creating Z-Read: {ex.Message}";
                _log.Error($"Error creating Z-Read: {ex.ToString()}"); 
                return false;
            }

            if (!PrintZRead(zRead))
            {
                _lastErrorString = $"Error printing Z-Read: {_lastErrorString}";
                _log.Error($"Error printing Z-Read: {_lastErrorString}");
                return false;
            }

            //Return the Z-Read ID as Z-Read number.
            zCloseNumber = zRead.ZreadId;

            //And off we go!
            return true;

        }

        bool PerformCashierClose(ref int dummyCashierCloseNumber)
        {
            //Cashier close - Some fiscal controllers and/or implementations need this.
            ClearLastError();
            dummyCashierCloseNumber = 0;
            return true;
        }

        bool FiscalDriverBase.IFiscalDriverBase.PerformXClose()
	    {
            //X-Close: Philippineans do not use this.
            ClearLastError();
            return true;
	    }

        bool FiscalDriverBase.IFiscalDriverBase.PrintNonFiscalDoc(
            IList<string> textLines,
            ref int nonFiscalDocNumber,
            bool dummyParam1,
            bool dummyParam2
           )
        {

            var retVal = false;
            var prtDriver = new DCS.Drivers.SacoaPrinters.WindowsPrinter.Driver(_driverConnectionString);
            var ticketData = new DCS.Drivers.SacoaPrinters.SacoaPrinterBase.TicketData();
            var nfdText = "";

            ClearLastError();

            try
            {

                //Convert textLines from IList to List and apply string.Join on the List specifying ENTER as delimiter
                nfdText = string.Join(Environment.NewLine, new List<string>(textLines));
                retVal = PrintNonFiscalDocPrivate(nfdText);

            }
            catch (System.Exception ex)
            {
                _lastErrorString = $"Error preparing to print Non-Fiscal doc: {ex.Message}";
                _log.Error($"Error preparing to print Non-Fiscal doc: {ex.ToString()}");
            }

            return retVal;

        }

        bool FiscalDriverBase.IFiscalDriverBase.PrintNonFiscalDoc(
            VBA.Collection textLines, 
            ref int docNumber, 
            bool dummyPrintHeader,
		    bool dummyCutPaper)
	    {

            /* 
             * Prints a non-fiscal doc. The VBA.Collection passed holds a text-line collection.
             * No need to pass back a coherent doc number for this driver.
             * Implemented the new DCS.Driver.SacoaPrinters.XXXXX interfaces.
             */

            var retVal = false;
            var prtDriver = new DCS.Drivers.SacoaPrinters.WindowsPrinter.Driver(_driverConnectionString);
            var ticketData = new DCS.Drivers.SacoaPrinters.SacoaPrinterBase.TicketData();
            var nfdText = "";

            ClearLastError();

            docNumber = 0;

            try
            {

                //Convert VBA.Collection to string (VBA.Collection lacks method .ToArray()).
                foreach (string line in textLines)
                {
                    nfdText += Environment.NewLine + line;
                }
                //Lop off first NewLine.
                nfdText = nfdText.Substring(Environment.NewLine.Length);

                retVal = PrintNonFiscalDocPrivate(nfdText);

            }
            catch (System.Exception ex)
            {
                _lastErrorString = $"Error preparing to print Non-Fiscal doc: {ex.Message}";
                _log.Error($"Error preparing to print Non-Fiscal doc: {ex.ToString()}");
            }

            return retVal;

        }

        bool PrintNonFiscalDocPrivate(string nfdText)
        {

            var retVal = false;
            var prtDriver = new DCS.Drivers.SacoaPrinters.WindowsPrinter.Driver(_driverConnectionString);
            var ticketData = new DCS.Drivers.SacoaPrinters.SacoaPrinterBase.TicketData();

            try
            {

                ticketData.DataDictionary.Add("NFD_TEXT", nfdText);
                prtDriver.PrintTicket(_nonfiscalPrintTemplate, ticketData);

                retVal = true;

            }
            catch (Exception ex)
            {
                _lastErrorString = $"Error printing Non-Fiscal doc: {ex.Message}";
                _log.Error($"Error printing Non-Fiscal doc: {ex.ToString()}");
            }

            return retVal;

        }

        void GetTransactionDTOTotals(
            FiscalDriverBase.Dto.TransactionDto xactDto,
            out decimal totalTaxAmount,
            out decimal returnsTotal,
            out decimal salesTotal)
        {

            /*
             * Totals are calculated in one of two ways depending on whether the transaction
             * is exempt or not. This is because POSEngine sends POS back an empty Taxes
             * collection when the transaction's exempt. Therefore, the foreach would not
             * sum values at all and the "TotalTaxAmount" would be zero.
             */
            totalTaxAmount = 0;
            returnsTotal = 0;
            salesTotal = 0;

            foreach(DCS.FiscalDriverBase.Card cd in xactDto.Cards)
            {
                foreach(DCS.FiscalDriverBase.Product pr in cd.Products)
                {

                    decimal prodAmount = pr.Quantity * pr.UnitPrice;
                    decimal mddrAmount = 0;

                    bool isReturn = (0 > prodAmount);

                    //Process products
                    if (isReturn)
                    {
                        returnsTotal += prodAmount;
                    }
                    else
                    {
                        salesTotal += prodAmount;
                    }

                    //Process product modders
                    foreach(DCS.FiscalDriverBase.Modifier md in pr.Modifiers)
                    {

                        mddrAmount = pr.Quantity * md.UnitPrice;

                        if (isReturn)
                        {
                            returnsTotal += mddrAmount;
                        }
                        else
                        {
                            salesTotal += mddrAmount;
                        }

                    }

                    //Now, process the product-defined taxes
                    decimal lineItemAmount = prodAmount + mddrAmount;
                    foreach (DCS.FiscalDriverBase.Tax tx in pr.Taxes)
                    {

                        decimal lineItemTax = 0;

                        if (DCS.FiscalDriverBase.Tax.TaxType.VATType == tx.Type)
                        {
                            //Discriminated taxes (VAT-Type)
                            lineItemTax = (lineItemAmount - (lineItemAmount / (1 + (tx.Rate / 100))));
                        }
                        else
                        {
                            //Non-discriminated taxes (SalesTax-Type)
                            lineItemTax = (lineItemAmount * (tx.Rate / 100));
                        }

                        totalTaxAmount += lineItemTax;

                    }

                }

            }

            //Above calculations try to calculate Tax Amount off the products'
            //definitions. This is useful when the transaction in exempt, because
            //the transaction's Taxes collection would be empty. But, if we are
            //provided with tax data at the transaction level, we should instead
            //use it.
            if(SalesInvoice.InvoiceTypesEnum.Normal == _currentTransaction.InvoiceCustomerData.CustomerType)
            {
                if (0 != xactDto.Taxes.Count)
                {
                    totalTaxAmount = 0;
                    foreach (DCS.FiscalDriverBase.Tax tx in xactDto.Taxes)
                    {
                        totalTaxAmount += tx.Amount;
                    }
                }
            }

        }

        bool FiscalDriverBase.IFiscalDriverBase.PrintTransaction(
            FiscalDriverBase.Dto.TransactionDto transaction, 
            ref int invoiceNumber, 
            bool dummyParam)
        {
            return PrintTransactionInternal(transaction, ref invoiceNumber);
        }

        private bool PrintTransactionInternal(
            DCS.FiscalDriverBase.Dto.TransactionDto xactDto,
            ref int invoiceNumber)
        {

            var retVal = false;

            var driver = new DCS.Drivers.SacoaPrinters.WindowsPrinter.Driver(_driverConnectionString);
            var ticketData = new DCS.Drivers.SacoaPrinters.SacoaPrinterBase.TicketData();
            var pathTemplate = string.Empty;
            var ticketDate = DateTime.Now;
            var invoicePayments = new List<Payment>();

            decimal returnsTotal = 0;
            decimal salesTotal = 0;
            decimal totalVatAmount = 0;

            GetTransactionDTOTotals(xactDto, out totalVatAmount, out returnsTotal, out salesTotal);

            //Transaction data: "XCT_" prefix
            ticketData.AddToDictionary("XCT_ID", xactDto.Id.ToString());
            ticketData.AddToDictionary("XCT_POS", Environment.GetEnvironmentVariable("ComputerName"));
            ticketData.AddToDictionary("XCT_POSREGNUMBER", _posRegistrationNumber.ToString());
            if (null != xactDto.Operator)
            {
                ticketData.AddToDictionary("XCT_CASHIERID", xactDto.Operator.ID.ToString());
                ticketData.AddToDictionary("XCT_CASHIERCARD", xactDto.Operator.Card.ToString());
                ticketData.AddToDictionary("XCT_CASHIERFNAME", xactDto.Operator.FirstName);
                ticketData.AddToDictionary("XCT_CASHIERLNAME", xactDto.Operator.LastName);
                ticketData.AddToDictionary("XCT_CASHIERNICK", xactDto.Operator.Nick);
            }
            else
            {
                ticketData.AddToDictionary("XCT_CASHIERID", NO_PRINT);
                ticketData.AddToDictionary("XCT_CASHIERCARD", NO_PRINT);
                ticketData.AddToDictionary("XCT_CASHIERFNAME", NO_PRINT);
                ticketData.AddToDictionary("XCT_CASHIERLNAME", NO_PRINT);
                ticketData.AddToDictionary("XCT_CASHIERNICK", NO_PRINT);
            }
            ticketData.AddToDictionary("XCT_DATE", ticketDate.ToString("MMM-dd-yyyy"));
            ticketData.AddToDictionary("XCT_TIME", ticketDate.ToString("HH:mm"));

            //Prepare a list of cards for this transaction.
            var cardsBlk = ticketData.AddRepeatBlock("Cards");
            foreach (DCS.FiscalDriverBase.Card cd in xactDto.Cards)
            {

                //Set up a new dictionary for this card and fill it.
                var cardItem = cardsBlk.AddNewItem();
                cardItem.AddToDictionary("CRD_CARDNUMBER",
                                         (1 > cd.Number) ?
                                         _nullCardProductName :
                                         _cardProductName + " " + cd.Number.ToString("N0")
                                        );
                cardItem.AddToDictionary("CRD_DEVICEID", (string.IsNullOrEmpty(cd.DeviceID)) ? NO_PRINT : cd.DeviceID);

                //Prepare a list of products for this card.
                var prodBlk = cardItem.CreateRepeatBlock("Products");
                foreach (DCS.FiscalDriverBase.Product pr in cd.Products)
                {

                    //Set up a new dictionary for this product and fill it.
                    var prodItem = prodBlk.AddNewItem();
                    prodItem.AddToDictionary("PRD_ID", pr.ID.ToString());
                    prodItem.AddToDictionary("PRD_BARCODE", NO_PRINT);  //prodItem.AddToDictionary("PRD_BARCODE", (string.IsNullOrEmpty(LinkedList.Barcode) ? NO_PRINT : li.Barcode));
                    prodItem.AddToDictionary("PRD_DESCRIPTION", pr.Description);
                    prodItem.AddToDictionary("PRD_QUANTITY", pr.Quantity.ToString());
                    prodItem.AddToDictionary("PRD_UNITPRICE", pr.UnitPrice.ToString(_currencyFormatString));
                    prodItem.AddToDictionary("PRD_TOTALPRICE", (pr.Quantity * pr.UnitPrice).ToString(_currencyFormatString));
                    prodItem.AddToDictionary("PRD_REQUEST", (string.IsNullOrEmpty(pr.SpecialRequest) ? NO_PRINT : pr.SpecialRequest));

                    //Prepare a list of modders for this product.
                    var mddrBlk = prodItem.CreateRepeatBlock("Modifiers");
                    foreach (DCS.FiscalDriverBase.Modifier md in pr.Modifiers)
                    {

                        //Set up a new dictionary for this modder and fill it.
                        var mddrItem = mddrBlk.AddNewItem();
                        mddrItem.AddToDictionary("MDR_ID", md.ID.ToString());
                        mddrItem.AddToDictionary("MDR_DESCRIPTION", md.Description);
                        mddrItem.AddToDictionary("MDR_UNITPRICE", md.UnitPrice.ToString(_currencyFormatString));
                        mddrItem.AddToDictionary("MDR_QUANTITY", pr.Quantity.ToString());
                        mddrItem.AddToDictionary("MDR_TOTALPRICE", (pr.Quantity * md.UnitPrice).ToString(_currencyFormatString));

                    }

                }

            }

            decimal totalTendered = 0;          //The total sum tendered for payment.
            decimal totalChange = 0;            //The change given back to the customer.

            //Prepare a list of payments for this transaction.
            var pmtsBlk = ticketData.AddRepeatBlock("Payments");
            foreach (DCS.FiscalDriverBase.Payment pm in xactDto.Payments)
            {

                //Fill paymentsMade list to add to invoice data later
                var invoicePayment = new Payment();
                invoicePayment.AmountPaid = pm.Paid;
                invoicePayment.AmountTendered = pm.Tendered;
                invoicePayment.AuthorizationCode = pm.AuthorizationCode;
                invoicePayment.CurrencyId = pm.Currency.ID;
                invoicePayment.CurrencyName = pm.Currency.Description;
                invoicePayment.CurrencyType = pm.Currency.TypeID;
                invoicePayment.ExchangeRatio = pm.Currency.ExchangeRatio;
                invoicePayments.Add(invoicePayment);

                var pmntItem = pmtsBlk.AddNewItem();
                pmntItem.AddToDictionary("PMT_PAID", invoicePayment.AmountPaid.ToString(_currencyFormatString));
                pmntItem.AddToDictionary("PMT_TENDERED", invoicePayment.AmountTendered.ToString(_currencyFormatString));
                pmntItem.AddToDictionary("PMT_CHANGE", pm.Change.ToString(_currencyFormatString));
                pmntItem.AddToDictionary("PMT_DESCRIPTION", invoicePayment.CurrencyName);
                pmntItem.AddToDictionary("PMT_AUTHORIZATIONCODE", invoicePayment.AuthorizationCode);

                //Keep track of actual total amount paid
                totalTendered += pm.Tendered;
                totalChange += pm.Change;

            }

            //Get new invoice number after no financial ops threw any exceptions
            invoiceNumber = GetNextInvoiceNumberAndIncrement();

            //Financials
            ticketData.AddToDictionary("XCT_INVOICE", invoiceNumber.ToString());
            ticketData.AddToDictionary("XCT_NETTOTAL", (xactDto.NetTotal).ToString(_currencyFormatString));
            ticketData.AddToDictionary("XCT_TAXTOTAL", totalVatAmount.ToString(_currencyFormatString));
            ticketData.AddToDictionary("XCT_SUBTOTAL", (xactDto.NetTotal + totalVatAmount).ToString(_currencyFormatString));
            ticketData.AddToDictionary("XCT_PRODUCTSSUM", (xactDto.NetTotal + totalVatAmount + totalVatAmount).ToString(_currencyFormatString));    //1014: For SC/PWD lineitem total.
            ticketData.AddToDictionary("XCT_DISCOUNT", xactDto.DiscountAmount.ToString(_currencyFormatString));
            ticketData.AddToDictionary("XCT_TOTALTOPAY", xactDto.TotalToPay.ToString(_currencyFormatString));
            ticketData.AddToDictionary("XCT_TENDERED", totalTendered.ToString(_currencyFormatString));
            ticketData.AddToDictionary("XCT_TOTALCHANGE", totalChange.ToString(_currencyFormatString));

            //Persist invoice data
            var invoice = new SalesInvoice
            {
                OperationDate = ticketDate,
                RegistrationNumber = _posRegistrationNumber,
                InvoiceNumber = invoiceNumber,
                CustomerName = _currentTransaction.InvoiceCustomerData.CustomerName,
                CustomerAddress = _currentTransaction.InvoiceCustomerData.CustomerAddress,
                SalesTotal = salesTotal,
                ReturnsTotal = returnsTotal,
                SalesDiscount = xactDto.DiscountAmount,
                CashierTransactionId = xactDto.Id,
                InvoiceId = _currentTransaction.InvoiceCustomerData.CustomerBeneficiaryNumber,
                InvoiceType = _currentTransaction.InvoiceCustomerData.CustomerType,
                HypotheticVatAmount = totalVatAmount,
                Payments = invoicePayments
                //Estos items ahora se calculan dentro de SalesInvoice en base al Hypothetic... ->
                //VatOfExemptSales = (_currentTransaction.InvoiceCustomerData.TipoCliente != SalesInvoice.InvoiceTypesEnum.Normal ? (productTaxAmt + moddersTaxAmt) : 0),
                //ZeroRatedSales = (_currentTransaction.InvoiceCustomerData.TipoCliente == SalesInvoice.InvoiceTypesEnum.Peza ? (productTaxAmt + moddersTaxAmt) : 0),
                //VatTotal = (_currentTransaction.InvoiceCustomerData.TipoCliente == SalesInvoice.InvoiceTypesEnum.Normal ? (productTaxAmt + moddersTaxAmt) : 0),				
            };

            try
            {
                _dao.SaveInvoice(invoice);
                _log.Info($"Invoice #{invoice.InvoiceNumber.ToString()} data saved to database.");
            }
            catch (Exception e)
            {
                _lastError = 513;
                _lastErrorString = $"Error saving invoice #{invoice.InvoiceNumber.ToString()} data to SQL: {e.ToString()}";
                return retVal;
            }

            //Customer data: "CST_" prefix - Set customer data and select template.
            switch (invoice.InvoiceType)
            {

                case SalesInvoice.InvoiceTypesEnum.Normal:
                    ticketData.AddToDictionary("CST_NAME", NO_PRINT);
                    ticketData.AddToDictionary("CST_ADDRESS", NO_PRINT);
                    ticketData.AddToDictionary("CST_PERSONID", NO_PRINT);
                    ticketData.AddToDictionary("CST_BENEFICIARYID", NO_PRINT);
                    pathTemplate = _normalPrintTemplate;
                    break;

                case SalesInvoice.InvoiceTypesEnum.Sc:
                    ticketData.AddToDictionary("CST_NAME", invoice.CustomerName);
                    ticketData.AddToDictionary("CST_ADDRESS", invoice.CustomerAddress);
                    ticketData.AddToDictionary("CST_PERSONID", _currentTransaction.InvoiceCustomerData.CustomerDocNumber);
                    ticketData.AddToDictionary("CST_BENEFICIARYID", _currentTransaction.InvoiceCustomerData.CustomerBeneficiaryNumber);
                    pathTemplate = _scpwdPrintTemplate;
                    break;

                case SalesInvoice.InvoiceTypesEnum.Pwd:
                    ticketData.AddToDictionary("CST_NAME", invoice.CustomerName);
                    ticketData.AddToDictionary("CST_ADDRESS", invoice.CustomerAddress);
                    ticketData.AddToDictionary("CST_PERSONID", _currentTransaction.InvoiceCustomerData.CustomerDocNumber);
                    ticketData.AddToDictionary("CST_BENEFICIARYID", _currentTransaction.InvoiceCustomerData.CustomerBeneficiaryNumber);
                    pathTemplate = _scpwdPrintTemplate;
                    break;

                case SalesInvoice.InvoiceTypesEnum.Organization:
                    ticketData.AddToDictionary("CST_NAME", NO_PRINT);
                    ticketData.AddToDictionary("CST_ADDRESS", NO_PRINT);
                    ticketData.AddToDictionary("CST_PERSONID", NO_PRINT);
                    ticketData.AddToDictionary("CST_BENEFICIARYID", NO_PRINT);
                    pathTemplate = _orgPrintTemplate;
                    break;

                default:
                    ticketData.AddToDictionary("CST_NAME", NO_PRINT);
                    ticketData.AddToDictionary("CST_ADDRESS", NO_PRINT);
                    ticketData.AddToDictionary("CST_PERSONID", NO_PRINT);
                    ticketData.AddToDictionary("CST_BENEFICIARYID", NO_PRINT);
                    pathTemplate = _normalPrintTemplate;
                    break;

            }

            //Finally, print
            try
            {

                retVal = driver.PrintTicket(pathTemplate, ticketData);
                if (retVal)
                {
                    _lastError = 0;
                    _lastErrorString = $"Invoice #{invoice.InvoiceNumber.ToString()} printed.";
                    _log.Info(_lastErrorString);
                }
                else
                {
                    _lastError = 513;
                    _lastErrorString = $"Low level printer driver failed to print invoice #{invoice.InvoiceNumber.ToString()} (driver error: {driver.LastErrorMsg})";
                    _log.Warn(_lastErrorString);
                }

            }
            catch (Exception ex)
            {

                _lastError = 513;
                _lastErrorString = $"Error printing invoice #{invoice.InvoiceNumber.ToString()}: {ex.Message}";
                _log.Error($"Error printing invoice #{invoice.InvoiceNumber.ToString()}: {ex.ToString()}");
                retVal = false;

            }

            return retVal;

        }

        private int GetNextInvoiceNumberAndIncrement()
        {

            int invoiceNumber;
            string sequenceFile = Path.Combine(_sysFolder, $"DCS\\Data\\Invoice_{Environment.MachineName}.Seq");

            //_log.debug($"Checking if exists sequence file {sequenceFile}")
            if (File.Exists(sequenceFile))
            {

                //_log.Debug("Sequence file DOES exist. Now reading next invoice number")
                using (var reader = new StreamReader(sequenceFile))
                {
                    var sequenceText = reader.ReadToEnd();

                    if (!int.TryParse(sequenceText, out invoiceNumber))
                    {
                        //_log.Warn($"Text read from sequence file: [{sequenceFile}], is not a valid invoice number. Resetting invoice number to [1]")			
                        invoiceNumber = 1;
                    }

                }

                using (var writer = new StreamWriter(sequenceFile))
                {
                    //_log.Debug("Writing incremented invoice number to sequence file")
                    writer.Write(invoiceNumber + 1);
                    writer.Flush();
                }

            }
            else
            {

                //_log.Warn($"Invoice sequence file [{sequenceFile}] DOES NOT exist. Creating it now. Current invoice number will be [1], and next invoice number of [2] will be written to sequence file")
                invoiceNumber = 1;
                using (var stream = File.Create(sequenceFile))
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write(2);
                        writer.Flush();
                    }
                }

            }

            return invoiceNumber;
        }

        private bool GetNewInvoiceTarget(ref InvoiceCustomerData invCustData)
        {

            //1.0.1.7: Return with normal and empty InvoiceCustomerData object to target Kiosks NORMAL-only sales
            if (_useOnlyNormalInvoicing)
            {

                invCustData = new InvoiceCustomerData();
                invCustData.CustomerType = SalesInvoice.InvoiceTypesEnum.Normal;
                return true;

            }
            else
            {

                //1.0.1.7: This was already-existing code for POS.
                using (var getFiscalDataDialog = new RequestFiscalDataForm())
                {

                    if (System.Windows.Forms.DialogResult.OK == getFiscalDataDialog.ShowDialog())
                    {
                        invCustData = getFiscalDataDialog.FiscalData;
                        return true;
                    }
                    else
                    {
                        invCustData = null;
                        return false;
                    }

                }

            }

        }

        bool FiscalDriverBase.IFiscalDriverBase.OpenTransaction(
            ref VBA.Collection fiscalParameters)
	    {

            /* Aca abre la transaccion, lo cual segun el driver puede incluir agregar algun record a una tabla
             * o (como en el caso de Filipinas) dar a elegir mediante un form el tipo de factura (normal, jubilado/discapacitado, peza).
             * En base al tipo de factura seleccionada hay que setear TaxExempt y DiscountId. El DiscountId para cada tipo de facturacion debera ser leido de algun INI en el Create
             * Si por algun motivo no se puede abrir la transaccion, ya sea porque 'NeedsZClose', la impresora no responde, no tiene papel, o simplemente en el form en pantalla el
             * cajero seleccionó 'cancel' OpenTransaction devolverá FALSE. Si está todo bien devuelve TRUE.
             * InvoiceCustomerData podrá tener mas datos en versiones nuevas de drivers. Yo comentado pongo lo que veo hasta hoy segun necesidades filipinas
             */

            ClearLastError();
            if(GetNewInvoiceTarget(ref _invoiceCustomerData))
            {

                var isTaxExempt = false;
                var discountId = 0;

                switch (_invoiceCustomerData.CustomerType)
                {
                    case SalesInvoice.InvoiceTypesEnum.Sc:
                        isTaxExempt = true;
                        discountId = _discountId;
                        break;

                    case SalesInvoice.InvoiceTypesEnum.Pwd:
                        isTaxExempt = true;
                        discountId = _discountId;
                        break;

                    case SalesInvoice.InvoiceTypesEnum.Organization:
                        isTaxExempt = true;
                        discountId = 0;
                        break;

                    default:
                        isTaxExempt = false;
                        discountId = 0;
                        break;
                }

                fiscalParameters.Add(isTaxExempt, "TaxExempt");
                fiscalParameters.Add(discountId, "DiscountId");

                _currentTransaction = new Transaction(_invoiceCustomerData);

                return true;

            }
            else
            {
                
                /*
                 * Do not allow POS to open the transaction, return meaningful text.
                 */
                _lastError = 513;
                _lastErrorString = "Data entry canceled by operator";

                return false;

            }

        }

        bool FiscalDriverBase.IFiscalDriverBase.OpenTransaction(
            ref IList<KeyValuePair<string, string>> transactionCommandsToApply)
        {
            ClearLastError();
            if (GetNewInvoiceTarget(ref _invoiceCustomerData))
            {

                var isTaxExempt = false;
                var discountId = 0;

                switch (_invoiceCustomerData.CustomerType)
                {
                    case SalesInvoice.InvoiceTypesEnum.Sc:
                        isTaxExempt = true;
                        discountId = _discountId;
                        break;

                    case SalesInvoice.InvoiceTypesEnum.Pwd:
                        isTaxExempt = true;
                        discountId = _discountId;
                        break;

                    case SalesInvoice.InvoiceTypesEnum.Organization:
                        isTaxExempt = true;
                        discountId = 0;
                        break;

                    default:
                        isTaxExempt = false;
                        discountId = 0;
                        break;
                }

                if(null == transactionCommandsToApply)
                {
                    transactionCommandsToApply = new List<KeyValuePair<string, string>>();
                }

                transactionCommandsToApply.Add(new KeyValuePair<string, string>("TaxExempt", isTaxExempt.ToString()));
                transactionCommandsToApply.Add(new KeyValuePair<string, string>("DiscountId", discountId.ToString()));

                _currentTransaction = new Transaction(_invoiceCustomerData);

                return true;

            }
            else
            {

                /*
                 * Do not allow POS to open the transaction, return meaningful text.
                 */
                _lastError = 513;
                _lastErrorString = "Data entry canceled by operator";

                return false;

            }

        }

        int FiscalDriverBase.IFiscalDriverBase.ProcessId()
        {
            return System.Diagnostics.Process.GetCurrentProcess().Id;
        }

        private string GetVersion()
        {

            //using System.Reflection;
            //using System.Diagnostics;

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fileVersionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);

            return (fileVersionInfo.ProductVersion);

        }

        public FiscalDriverBase.PrinterStatus GetPrinterStatus()
        {
            return (FiscalDriverBase.PrinterStatus.Ok);
        }

    }

}

