﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace DCS.FiscalDriver.Ph
{

    public partial class RequestFiscalDataForm : Form
	{

        private readonly InvoiceCustomerData _fiscalData = new InvoiceCustomerData();

		public InvoiceCustomerData FiscalData => _fiscalData;
		
		public RequestFiscalDataForm()
        {
            InitializeComponent();

            //Some internationalization
            var setting = "";
            ConfigEngine.Engine config = new ConfigEngine.Engine("POS", "PrtDrv_FcalPh.Cfg");

            this.Text = "";

            setting = config.GetItem("International", "Box Title Customer Type", "Customer Type");
            this.fmeCustomerType.Text = setting;
            setting = config.GetItem("International", "Label Customer Type Normal", "Normal");
            this.optCustNormal.Text = setting;
            setting = config.GetItem("International", "Label Customer Type Senior", "Senior");
            this.optCustSenior.Text = setting;
            setting = config.GetItem("International", "Label Customer Type Disabled", "Disabled");
            this.optCustDisabled.Text = setting;
            setting = config.GetItem("International", "Label Customer Type Organization", "Organization");
            this.optCustOrg.Text = setting;

            setting = config.GetItem("International", "Box Title Customer Data", "Customer Data");
            this.fmeCustomerData.Text = setting;
            setting = config.GetItem("International", "Label Customer Data Name", "Name");
            this.lblName.Text = setting;
            setting = config.GetItem("International", "Label Customer Data Address", "Address");
            this.lblAddress.Text = setting;
            setting = config.GetItem("International", "Label Customer Data Contributor ID", "Contributor ID");
            this.lblCustID.Text = setting;
            setting = config.GetItem("International", "Label Customer Data Beneficiary ID", "Beneficiary ID");
            this.lblBenefID.Text = setting;

            setting = config.GetItem("International", "Button Title Accept", "Accept");
            this.cmdOK.Text = setting;
            setting = config.GetItem("International", "Button Title Cancel", "Cancel");
            this.cmdCancel.Text = setting;

            EnableCustomerDataEntry();

        }

		private void EnableCustomerDataEntry()
		{

            bool enableUse = false;

            if (optCustSenior.Checked || optCustDisabled.Checked)
            {
                enableUse = true;
            }
            else
            {
                txtName.Text = "";
                txtAddress.Text = "";
                txtCustID.Text = "";
                txtBenefID.Text = "";
            }

            txtName.BackColor = SystemColors.Window;
            txtName.ForeColor = SystemColors.ControlText;
            txtAddress.BackColor = SystemColors.Window;
            txtAddress.ForeColor = SystemColors.ControlText;
            txtCustID.BackColor = SystemColors.Window;
            txtCustID.ForeColor = SystemColors.ControlText;
            txtBenefID.BackColor = SystemColors.Window;
            txtBenefID.ForeColor = SystemColors.ControlText;

            lblName.Enabled = enableUse;
            txtName.Enabled = enableUse;
            lblAddress.Enabled = enableUse;
            txtAddress.Enabled = enableUse;
            lblCustID.Enabled = enableUse;
            txtCustID.Enabled = enableUse;
            lblBenefID.Enabled = enableUse;
            txtBenefID.Enabled = enableUse;

        }
      
        private void cmdOK_Click(object sender, EventArgs e)
        {

            bool accept = true;

            if (optCustDisabled.Checked || optCustSenior.Checked)
            {

                if (optCustDisabled.Checked)
                {
                    _fiscalData.CustomerType = Domain.SalesInvoice.InvoiceTypesEnum.Pwd;
                }
                else
                {
                    _fiscalData.CustomerType = Domain.SalesInvoice.InvoiceTypesEnum.Sc;
                }

                if (!String.IsNullOrEmpty(txtName.Text))
                {
                    _fiscalData.CustomerName = txtName.Text;
                }
                else
                {
                    // si falla la validación, genero msj y anulo el evento click
                    //MessageBox.Show("El campo Nombre es obligatorio");
                    txtName.BackColor = Color.Red;
                    txtName.ForeColor = Color.White;
                    accept = false;
                }

                if (!String.IsNullOrEmpty(txtAddress.Text))
                {
	                _fiscalData.CustomerAddress = txtAddress.Text;
                }
                else
                {
                    // si falla la validación, genero msj y anulo el evento click
                    //MessageBox.Show("El campo Dirección es obligatorio");
                    txtAddress.BackColor = Color.Red;
                    txtAddress.ForeColor = Color.White;
                    accept = false;
                }

                if (!String.IsNullOrEmpty(txtBenefID.Text))
                {
	                _fiscalData.CustomerBeneficiaryNumber = txtBenefID.Text;
                }
                else
                {
                    // si falla la validación, genero msj y anulo el evento click
                    //MessageBox.Show("El campo Iden. Contribuyente es obligatorio");
                    txtBenefID.BackColor = Color.Red;
                    txtBenefID.ForeColor = Color.White;
                    accept = false;
                }

                if (!String.IsNullOrEmpty(txtCustID.Text))
                { 
	                _fiscalData.CustomerDocNumber = txtCustID.Text;
                }
                else
                {
                    // si falla la validación, genero msj y anulo el evento click
                    //MessageBox.Show("El campo Iden. Beneficiario es obligatorio");
                    txtCustID.BackColor = Color.Red;
                    txtCustID.ForeColor = Color.White;
                    accept = false;
                }

            }
            else if (optCustOrg.Checked)
            {
	            _fiscalData.CustomerType = Domain.SalesInvoice.InvoiceTypesEnum.Organization;
                // la operación debe ser autorizada por un encargado (esto queda ver como implementarlo)
            }
            else
            {
	            _fiscalData.CustomerType = Domain.SalesInvoice.InvoiceTypesEnum.Normal;
            }
            
            //this.Dispose(); -> Dispose lo saca de la memoria y necesito leer el resultado desde afuera
            if (accept)
            {
                this.Hide();
            }
            else
            {
                this.DialogResult = DialogResult.None;
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            //this.Dispose();
            this.DialogResult = DialogResult.Cancel;
        }

        private void optCustSenior_CheckedChanged(object sender, EventArgs e)
        {
            EnableCustomerDataEntry();
        }

        private void optCustNormal_CheckedChanged(object sender, EventArgs e)
        {
            EnableCustomerDataEntry();
        }

        private void optCustDisabled_CheckedChanged(object sender, EventArgs e)
        {
            EnableCustomerDataEntry();
        }

        private void optCustOrg_CheckedChanged(object sender, EventArgs e)
        {
            EnableCustomerDataEntry();
        }
    }
}
