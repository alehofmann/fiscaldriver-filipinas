﻿namespace DCS.FiscalDriver.Ph.Domain
{
	public class ZReadPaymentItem
	{
		public string CurrencyName { get; }
		public decimal TotalAmount { get; }

		public ZReadPaymentItem(string currencyName, 
                                decimal totalAmount)
		{
			CurrencyName = currencyName;
			TotalAmount = totalAmount;
		}

	}
}
