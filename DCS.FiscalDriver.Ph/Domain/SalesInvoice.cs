﻿using System;
using System.Collections.Generic;

namespace DCS.FiscalDriver.Ph.Domain
{
	public class SalesInvoice
	{
		public enum InvoiceTypesEnum
		{
			Normal = 1,
			Sc = 2,
			Pwd = 3,
			Organization = 4
		}

		//public Payment Payment { get; set; }
		public DateTime OperationDate { get; set; }

		public int RegistrationNumber { get; set; }

        public int InvoiceNumber { get; set; }

        public string CustomerName { get; set; }

        public string CustomerAddress { get; set; }

        public decimal SalesTotal { get; set; }

        public decimal ReturnsTotal { get; set; }

        public decimal SalesDiscount { get; set; }

		public decimal VatOfExemptSales
		{
			get
			{
				return ((InvoiceType == InvoiceTypesEnum.Pwd || InvoiceType == InvoiceTypesEnum.Sc) ? HypotheticVatAmount : 0);
			}
		}

		public decimal VatTotal
		{
			get { return ((InvoiceType == InvoiceTypesEnum.Normal) ? HypotheticVatAmount : 0); }
		}

		public int CashierTransactionId { get; set; }

        public string InvoiceId { get; set; }

        public InvoiceTypesEnum InvoiceType { get; set; }

		public decimal HypotheticVatAmount { get; set; }

		public decimal VatableSales
		{
			get
			{
				if (InvoiceType == InvoiceTypesEnum.Normal)
				{
					return (SalesTotal - SalesDiscount - VatTotal);
				}

				return 0;
				
			}
		}

		public decimal ZeroRatedSales
		{
			get
			{
				if (InvoiceType == InvoiceTypesEnum.Organization)
				{
					return (SalesTotal - SalesDiscount);
				}
				else
				{
					return 0;
				}

			}
		}

		public decimal VatExemptSales
		{
			get
			{
				if (InvoiceType == InvoiceTypesEnum.Pwd || InvoiceType == InvoiceTypesEnum.Sc)
				{
					return (SalesTotal - SalesDiscount - VatOfExemptSales);
				}
				else
				{
					return 0;
				}

			}
		}

        public List<Payment> Payments { get; set; }

    }
}

		
	

