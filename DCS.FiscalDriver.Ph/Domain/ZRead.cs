﻿using System;
using System.Collections.Generic;

namespace DCS.FiscalDriver.Ph.Domain
{
	public class Zread
	{
		public Zread()
		{
		}

        public List<ZReadPaymentItem> Payments { get; set; } = new List<ZReadPaymentItem>();

        public string ComputerName { get; set; }
		public DateTime ZreadDate { get; set; }
		public int ZreadId { get; set; }
		//public DateTime IssueDate { get; set; }
		public decimal OpeningSalesReading { get; set; }
		public decimal ClosingSalesReading { get; set; }
		public long FirstInvoiceNumber{ get; set; }
		public long LastInvoiceNumber { get; set; }
		public decimal SalesTotal { get; set; }
		
		public decimal ReturnsTotal { get; set; }
		public decimal DiscountsTotal { get; set; }
		public decimal VatOfExemptSales { get; set; }
		public decimal VatAmount { get; set; }
		public decimal VatableSales { get; set; }
		public decimal ZeroRatedSales { get; set; }
		public decimal VatExemptSales { get; set; }
		public decimal TotalNetSales { get; set; }
		public decimal NetSales { get; set; }

	}
}


