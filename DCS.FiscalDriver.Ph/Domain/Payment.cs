﻿namespace DCS.FiscalDriver.Ph.Domain
{
    public class Payment
    {

        /* This class represents a POS payment, be it from DCSMultipay or a single payment.
         * The idea is to detach DCSPOSUtility from the driver by using collections/lists of
         * this class instead of DCSPOSUtility.clsPOSPayment.
         */

        public decimal AmountPaid{ get; set; }

        public decimal AmountTendered { get; set; }

        public int CurrencyId { get; set; }

        public int CurrencyType { get; set; }

        public string CurrencyName { get; set; }

        public decimal ExchangeRatio { get; set; }

        public string AuthorizationCode { get; set; }

    }
}
