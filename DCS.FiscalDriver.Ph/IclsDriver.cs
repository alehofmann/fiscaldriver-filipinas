namespace DCS.FiscalDriver.Ph
{
    [System.Runtime.InteropServices.ComVisible(true)]
    public interface IclsDriver
	{

        bool Create(string drive, string dummy = "");

        bool DriverPrintsFromTransactionObject();

        int LastError();

        string LastErrorString();

        bool NeedsZClose();

        bool PerformZClose(ref int zCloseNumber, 
			bool dummyPrintHeaderAndFooter = true);

        bool PerformCashierClose(ref int dummyCashierCloseNumber);

        bool PerformXClose();

        bool PrintNonFiscalDoc(VBA.Collection textLines,
			ref int docNumber,
			bool dummyPrintHeader = true,
			bool dummyCutPaper = true);

#if DEBUG
        bool PrintTransaction(DCSPOSUtility.clsPOSXaction transaction,
			ref int invoiceNumber,
			bool dummyCutPaper = true);
#else
        bool PrintTransaction(object transaction,
			ref int invoiceNumber,
			bool dummyCutPaper = true);
#endif

        bool OpenTransaction(ref VBA.Collection fiscalParameters);

        int ProcessID();

	}
}