﻿//Must add packages "NUnit" and "NUnit3TestAdapter" for this module to work.

namespace DriverTester
{

    [NUnit.Framework.TestFixture]
    public class DriverTester
    {

        private DCS.FiscalDriverBase.IFiscalDriverBase _driver;

        [NUnit.Framework.SetUp]
        public void ThisRunsBeforeAnyTest()
        {

            _driver = new DCS.FiscalDriver.Ph.Driver();
            _driver.Create(System.Environment.GetEnvironmentVariable("NW"));

        }

        [NUnit.Framework.TearDown]
        public void RunsAfterallTests()
        {
            _driver = null;
        }

        [NUnit.Framework.Test]
        public void TestIniGet()
        {

            string setting = INIReader.INIGet("General",
                                              "Discount Id",
                                              "",
                                              "\\\\192.168.1.16\\Dev\\Stores\\ML\\DCS\\Config\\POS\\PrtDrv_FcalPH.Cfg"
                                             );

        }

    }
}
